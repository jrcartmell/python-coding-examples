#!/usr/bin/env python3
"""
    Purpose: Some examples of recursion, some using various forms of caching to speed up the recursion.
    Syntax:  python recursion.py
    Use: Free use, no licensing required.
    Details: xTreemAnalytics.  Copyright 2020 by J. Cartmell.
"""

import time
from functools import lru_cache

# Simple factorial function.  If the value is 1, it returns 1, otherwise it returns
# product of the number and (number-1)!.
def factorial(num):
    if num == 1:
        return num
    else:
        return factorial(num-1) * num

# Method to calculate fibonacci sequence for a specific number.  If the number is 1 or 0,
# that is returned, otherwise, method is called twice with num-1 and num-2.  NO CACHING!!
def fibs_slow(num):
    import time
    time.sleep(1)
    if num == 0:
        return 0
    elif num == 1:
        return 1
    else:
        return fibs(num-1) + fibs(num-2)

# Method to calculate fibonacci sequence for a specific number.  If the number is 1 or 0,
# that is returned, otherwise, method is called twice with num-1 and num-2.  Caching decorator
# is used to save the already computed values (to speed things up).
@lru_cache(maxsize=None)
def fibs(num):
    import time
    time.sleep(1)
    if num == 0:
        return 0
    elif num == 1:
        return 1
    else:
        return fibs(num-1) + fibs(num-2)

# Method to calculate fibonacci sequence.  This uses a dictionary to store the values, rather than
# using the decorator.
the_struct = {}
def fibs_alt(num):
    import time
    try:
        return the_struct[num]
    except:
        time.sleep(1)
        if num == 0:
            the_struct[num] = 0
            return 0
        elif num == 1:
            the_struct[num] = 1
            return 1
        else:
            the_val = fibs_alt(num-1)
            the_struct[num-1] = the_val
            the_other_val = fibs_alt(num-2)
            the_struct[num-2] = the_other_val
            return the_val + the_other_val 

# Method to sum up the numbers from 1 to 10.  Point here is that accumulated sum is passed
# through the instances of the method.
def sum_recursive(current_number, accumulated_sum):
    if current_number == 10:
        # Once we reach 10, return the accumulated sum.
        return accumulated_sum
    else:
        # Recursive case
        # Thread the state through the recursive call
        return sum_recursive(current_number + 1, accumulated_sum + current_number)


# Call the factorial method with values ranging from 1 to 20.
print('Example of recursion to find factorial of 1 to 20.')
for val in range(1,21):
    print('{}! = {:,}'.format(val, factorial(val)))

print('\n')

# This should be slowest.
start = time.time()
the_val = fibs_slow(10)
end = time.time()
print('Using Fibonacci sequence with no caching to find 10th element of sequence ({}) took {} seconds.'.format(the_val, round(end-start, 2)))

# This should be faster, since it using caching.
start = time.time()
the_val = fibs(10)
end = time.time()
print('Using Fibonacci sequence with decorator caching to find 10th element of sequence ({}) took {} seconds.'.format(the_val, round(end-start, 2)))

# This is fastest since all the values were already cached, allowing the method to just return the values already calculated.
start = time.time()
the_val = fibs(10)
end = time.time()
print('Using Fibonacci sequence with decorator caching to find 10th element of sequence ({}) took {} seconds.'.format(the_val, round(end-start, 5)))

# This should be faster, since it using caching, but not as fast as the decorator.
start = time.time()
the_val = fibs_alt(10)
end = time.time()
print('Using Fibonacci sequence with dictionary caching to find 10th element of sequence ({}) took {} seconds.'.format(the_val, round(end-start, 2)))

# This is faster since all the values were already cached.
start = time.time()
the_val = fibs_alt(10)
end = time.time()
print('Using Fibonacci sequence with dictionary caching to find 10th element of sequence ({}) took {} seconds.'.format(the_val, round(end-start, 5)))

print('\n')

# Print out the resurvice sum.  This just demonstrates passing the initial sum and seeing that it gets passed through the subsequent iterations.
print('Sum of integers between 1 and 10 is {}.'.format(sum_recursive(1, 0)))


