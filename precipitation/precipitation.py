"""
    Purpose: Demonstrate matplotlib using precipitation data from NWS. 
             Data is sourced from: https://water.weather.gov/precip/archive/
    Input:   None
    Output:  None
    Syntax:  python precipitation.py
    Details: xTreem Analytics.  Copyright 2020 by J. Cartmell.
"""

from mpl_toolkits.basemap import Basemap, cm
import matplotlib.pyplot as plt
import netCDF4
import numpy as np

def main():
    # Set the font size of the various entities on the plot.
    axis_font_size = 12
    label_font_size = 10
    title_font_size = 18

    # Read in the data from the NetCDF file.  This file was 
    # downloaded from https://water.weather.gov/precip/archive/.
    net_cdf_data = netCDF4.Dataset('nws_precip_conus_20170627.nc')

    # Extract the precipitation from the data and scale it.
    prcpvar = net_cdf_data.variables['amountofprecip']
    data = prcpvar[:]/100

    # Extract the latitude and longitude corners from the dataset.
    lat_corners = net_cdf_data.variables['lat'][:]
    lon_corners = -net_cdf_data.variables['lon'][:]

    # Extract the true latitude and longitude from the data set.
    lat_0 = net_cdf_data.variables['true_lat'].getValue()
    lon_0 = -net_cdf_data.variables['true_lon'].getValue()

    # create figure and axes instances
    fig = plt.figure(figsize=(10, 8), facecolor='grey')    

    # Create the basemap instance.
    map = Basemap(projection='stere',
                             lon_0=lon_0, lat_0=90, 
                             llcrnrlat=lat_corners[0], urcrnrlat=lat_corners[2],
                             llcrnrlon=lon_corners[0], urcrnrlon=lon_corners[2],
                             resolution='c')

    # Draw boundaries around the various landmark designations.
    map.drawcoastlines()
    map.drawstates()
    map.drawcountries()
    map.drawcounties()

    # Draw the latitude grid lines.
    lat_lines = np.arange(0, 90, 10)
    lat_axis_pos = [1, 0, 0, 0]
    map.drawparallels(lat_lines, labels=lat_axis_pos, fontsize=axis_font_size)

    # Draw the longitude grid lines.
    lon_lines = np.arange(180, 360, 10)
    lon_axis_pos = [0, 0, 0, 1]
    map.drawmeridians(lon_lines, labels=lon_axis_pos, fontsize=axis_font_size)

    # Get the number of x and y points.
    nx = data.shape[1]
    ny = data.shape[0]

    # Convert the number of x's and y's to 
    lons, lats = map.makegrid(nx, ny) # get lat/lons of ny by nx evenly space grid.
    x, y = map(lons, lats) # compute map proj coordinates.

    # Draw the geometric shapes on the graph using the buckets to shade the various contours.
    buckets = [0, 5, 10, 15, 20, 25, 50, 100, 150, 250, 500, 750]
    contours = map.contourf(x, y, data, buckets, cmap=cm.s3pcpn)

    # Add legend at bottom
    cbar = map.colorbar(contours, location='bottom', pad='10%')
    cbar.set_label('mm', fontsize=label_font_size)

    # Add title.
    the_date = '{}-{}-{} {}'.format(prcpvar.dateofdata[4:6], prcpvar.dateofdata[6:8], prcpvar.dateofdata[0:4], prcpvar.dateofdata[8:])
    plt.title('{} for period ending @ {}'.format(prcpvar.long_name, the_date), fontsize=title_font_size)

    # Display the figure.
    plt.show()

if __name__ == "__main__":
    main()

