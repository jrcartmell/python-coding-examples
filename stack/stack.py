#!/usr/bin/env python3
"""
    Purpose: To demonstrate the use of a stack to make sure a string
             is parenthesis balanced.
    Syntax:  python stack.py
    Use: Free use, no licensing required.
    Details: xTreemAnalytics.  Copyright 2021 by J. Cartmell.
"""
import collections
 
def parenthesis_checker(the_string):
    result = True
    stack = collections.deque()
    for item in the_string:
        if item == '(':
            stack.append(item)  # Push onto the stack
        elif item == ')':
            try:
                stack.pop() # Pop off the stack.
            except:
                # If there is nothing to pop, there are too many ")"
                # so set the result to False and exit the for loop.
                result = False
                break

    # If we processed the entire string and there is still something
    # left on the stack, there are too many "(", so set the result to 
    # False.
    if len(stack) > 0:
        result = False

    # Return the result
    return result

    
# Try some valid sets of ()
the_string = '(dfsdfsd)'
print('String: {}   Balanced: {}'.format(the_string, parenthesis_checker(the_string)))

the_string = '((dfsdfsd))'
print('String: {}   Balanced: {}'.format(the_string, parenthesis_checker(the_string)))

the_string = '(sdfsdf)(sdfsdf)'
print('String: {}   Balanced: {}'.format(the_string, parenthesis_checker(the_string)))

# Try some invalid sets of ()
the_string = '(dfsdfsd))'
print('String: {}   Balanced: {}'.format(the_string, parenthesis_checker(the_string)))

the_string = '((dfsdfsd)'
print('String: {}   Balanced: {}'.format(the_string, parenthesis_checker(the_string)))

the_string = '(dfsdfsd))(sdfs'
print('String: {}   Balanced: {}'.format(the_string, parenthesis_checker(the_string)))
