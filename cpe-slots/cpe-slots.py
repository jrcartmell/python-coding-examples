"""
    Purpose: Script to demonstrate placing CPEs in a linked list of time slots, 
             maximizing the distance between CPEs in the same time slot.
    Output:  Text file that shows the linked list and the placement of CPEs 
             within the slots that comprise the linked list.
    Syntax:  python cpe-slots.py
    Details: xTreem Analytics.  Copyright 2020 by J. Cartmell.
"""

# Define the node class - the linked list is comprised of these nodes.
class Node(object):
    def __init__(self, d, n=None):
        self.data = d
        self.next_node = n

    def get_next(self):
        return self.next_node

    def set_next(self, n):
        self.next_node = n

    def get_data(self):
        return self.data

    def set_data(self, d):
        self.data = d

# Define the linked list class.        
class LinkedList(object):
    def __init__(self, r = None):
        self.root = r
        self.size = 0

    def without_keys(d, keys):
        return {x: d[x] for x in d if x not in keys}

    def get_size(self):
        return self.size

    def add_slot(self, d):
        new_node = Node(d, self.root)
        self.root = new_node
        self.size += 1

    def add_cpe(self, cpeToAdd, day, timeOfSlot):
        this_node = self.root
        found = False
        while this_node and not found:
            slot = this_node.get_data()
            if slot['Day'] == day and slot['Start Time'] == timeOfSlot:
                found = True
            else:
                this_node = this_node.get_next()        
        if found:
            found_slot = this_node.get_data()
            found_slot['CPEs'][cpeToAdd['macAddress']] = LinkedList.without_keys(cpeToAdd, 'macAddress')
            found_slot['Zones'].append(cpeToAdd['zoneTag'])
            this_node.set_data(found_slot)
            return True
        else:
            return False

    def find_slot(self, day, startTime):
        this_node = self.root
        while this_node:
            slot = this_node.get_data()
            if slot['Day'] == day and slot['Start Time'] == startTime:
                return slot
            else:
                this_node = this_node.get_next()        
        return None
        
def main():
    import csv
    import math
    import random
    from random import shuffle
    import operator
    import collections
    import time

    # Set the maximum dimensions of the cube that will container the CPEs.
    cluster_max_z = 4
    cluster_max_x = 25
    cluster_max_y = 20

    # Capture the module start time, so we can determine how long the algorithm takes.
    module_start = time.time()
    
    # Create simulated data.
    list_of_cpes = []
    max_cpes = 500
    
    # Initialize cpe number to 1.
    num_cpe = 1
    
    # Set the MAC address digits, want specific range of MAC addresses.
    block1 = 10
    block2 = 10
    block3 = 10
    block4 = 10
    block5 = 10
    block6 = 10
    while num_cpe <= max_cpes:
        # Increment simulated CPE number.
        num_cpe += 1
    
        # Create a simulated CPE.
        cpe = {}
    
        # Put the simulated CPE in a random location.
        cpe['zoneTagX'] = random.randint(1,cluster_max_x)
        cpe['zoneTagY'] = random.randint(1,cluster_max_y)
        cpe['zoneTagZ'] = random.randint(1,cluster_max_z)
        cpe['zoneTag'] = 'Z_{}_{}_{}'.format(cpe['zoneTagX'], cpe['zoneTagY'], cpe['zoneTagZ'])
    
        # Assign a MAC address.
        cpe['macAddress'] = '{}:{}:{}:{}:{}:{}'.format(block1, block2, block3, block4, block5, block6)
        block1 += 1
        if block1 > 99:
            block1 = 10
            block2 += 1
        if block2 > 99:
            block2 = 10
            block3 += 1
        if block3 > 99:
            block3 = 10
            block4 += 1
        if block4 > 99:
            block4 = 10
            block5 += 1
        if block5 > 99:
            block5 = 10
            block6 += 1
        if block6 > 99:
            block6 = 10
    
        # Add this simualted CPE to the list of CPEs.
        list_of_cpes.append(cpe)
    
    print('Done creating CPEs.')
        
    # Instantiate the Linked List of time slots.
    tSlotList = LinkedList()
    
    # Configure the time slots (number of days, slots per day, time between slots, etc.)
    timeSlotsPerDay = 25   # Number of slots per day
    days = 39              # Days before repeat
    startTime = 180        # Minute to start
    timeBetweenSlots = 5   # Minutes
    
    # Configure the algorithm parameters.
    max_attempts = timeSlotsPerDay * days     # Maximum number of attempts to try before just picking "any" slot.
    floor_difference = 1   # Floor difference - don't pick a slot if AP in slot that is in the zone above or below of current AP.
    x_difference = 1       # X difference - don't pick a slot if AP in slot is in the zone to the left or right of current AP.
    y_difference = 1       # Y difference - don't pick a slot if AP in slot is in the zone on top or under of current AP.
    relax_attempts = int(max_attempts*.85)  # Number of attempts before algorithm loosens the adjacent parameters.
    
    # Create a linked list with one entry per time slot per day.  This is the grid that allows for an action on CPEs,
    # requiring that the CPEs are placed physically spread out, i.e. you can't take the same action on adjacent CPEs.
    i = 1
    while i <= days:
        j = 1
        while j <= timeSlotsPerDay:
            tSlotList.add_slot({'Zones': [], 'CPEs': {}, 'Day': i , 'Start Time': j})
            j += 1
        i += 1
    
    print('Done creating CPE Action Grid.')
    
    # Drop each CPE into a time period that has no CPEs in the same or adjacent time slot.  If it is "difficult" to find
    # such a slot, relax the criteria to only be the same time slot.  Failing this, just put the CPE into a slot.
    # Initialize a number of counters to monitor the activity of the algorithm.
    got_to_max = 0
    relaxed_criteria = 0
    times_look_again = 0
    cpes_processed = 0
    for this_cpe in list_of_cpes:
        # For this CPE, set the adjacent zone tags.  This is the six zones adjacent to the zone the CPE is in.
        # Determine this by adding/subtracting 1 to the floor, x and y tags to get the "adjacent" zones.
        # We want to make sure that we don't put this CPE in a time slot that has any CPEs in any of the
        # adjacent zones or the zone of this CPE.
        min_floor = max(int(this_cpe['zoneTagZ']) - floor_difference, 0)
        max_floor = min(int(this_cpe['zoneTagZ']) + floor_difference, cluster_max_z)
        min_floor_tag = 'Z_{}_{}_{}'.format(this_cpe['zoneTagX'], this_cpe['zoneTagY'], min_floor)
        max_floor_tag = 'Z_{}_{}_{}'.format(this_cpe['zoneTagX'], this_cpe['zoneTagY'], max_floor)
        min_x = max(int(this_cpe['zoneTagX']) - x_difference, 0)
        max_x = min(int(this_cpe['zoneTagX']) + x_difference, cluster_max_x)
        min_x_tag = 'Z_{}_{}_{}'.format(min_x, this_cpe['zoneTagY'], this_cpe['zoneTagZ'])
        max_x_tag = 'Z_{}_{}_{}'.format(max_x, this_cpe['zoneTagY'], this_cpe['zoneTagZ'])
        min_y = max(int(this_cpe['zoneTagY']) - y_difference, 0)
        max_y = min(int(this_cpe['zoneTagY']) + y_difference, cluster_max_y)
        min_y_tag = 'Z_{}_{}_{}'.format(this_cpe['zoneTagX'], min_y, this_cpe['zoneTagZ'])
        max_y_tag = 'Z_{}_{}_{}'.format(this_cpe['zoneTagX'], max_y, this_cpe['zoneTagZ'])
        adjacent = [min_floor_tag, max_floor_tag, min_x_tag, max_x_tag, min_y_tag, max_y_tag]
        
        # Pick a random day and time slot.
        candidateDay = random.randint(1, days)
        candidateSlot = random.randint(1, timeSlotsPerDay)
    
        # Reset/set a number of parameters used while trying to place this CPE in a time slot.
        attempts = 0
        one_shot = True
        done = False
        
        # Keep looking for a time slot until one is 
        while attempts < max_attempts and not done:
            # Get the current slot.
            current_slot = tSlotList.find_slot(candidateDay, candidateSlot)
            
            # Get the list of zones that are already placed in this slot.  If there are no CPEs this is an empty list.
            zone_list = current_slot['Zones']
            
            # If this CPE's zone or at least one of the adjacent zones of this CPE are already in the slot, only use this
            # slot if we have exhausted all the time slots.  This logic will only place an CPE in a zone if either of the 
            # following occurs: there is no CPE already in the time slot that is close to this CPE or the maximum number
            # of attempts have been attempted.  The former is a good case while the latter is the case of last resorts, 
            # the algorithm can't find a zone, so just pick one.
            if this_cpe['zoneTag'] in zone_list or min_floor_tag in zone_list or max_floor_tag in zone_list or min_x_tag in zone_list or max_x_tag in zone_list or min_y_tag in zone_list or max_y_tag in zone_list:
                attempts += 1       # Increase the number of attempts.
                times_look_again += 1   # Count the number of times we had to keep looking this is an interesting stat.
                if attempts >= max_attempts:
                    got_to_max += 1     # Got to max, so just use the candidate day and time slot.
                    print('got to max for zone = {}, processed {} CPEs so far'.format(this_cpe['zoneTag'], cpes_processed))
                    tSlotList.add_cpe(this_cpe, candidateDay, candidateSlot)
                    done = True         # Indicate that algorithm is done with this CPE.
                    cpes_processed += 1  # Increment number of CPEs processed.
            else:
                # No adjacent CPE is already in this slot, so assign this CPE to this day and this slot period.
                tSlotList.add_cpe(this_cpe, candidateDay, candidateSlot)
                done = True         # Indicate that algorithm is done with this CPE.
                cpes_processed += 1  # Increment number of CPEs processed.
                
            # Check if we have tried a lot of time periods already.  If they exceed a limit, relax the adjacent criteria.
            # Only exclude a time slot if the candidate day/slot has an CPE that is above, below, or in the same zone as this CPE.
            # Only need to do this once, so a one shot flag is used and this only needs to be done if the above logic did not
            # find a day/slot.
            if attempts >= relax_attempts and one_shot and not done:
                one_shot = False    # Reset the one shot flag.
                relaxed_criteria += 1   # Increment the count as this is a good statistic to look at how "hard" the algorithm is working.
                print('relaxing criteria, processed {} CPEs so far'.format(aps_processed))
                min_floor = int(this_cpe['zoneTagZ']) - 1
                max_floor = int(this_cpe['zoneTagZ']) + 1
                min_floor_tag = 'Z_{}_{}_{}'.format(this_cpe['zoneTagX'], this_cpe['zoneTagY'], min_floor)
                max_floor_tag = 'Z_{}_{}_{}'.format(this_cpe['zoneTagX'], this_cpe['zoneTagY'], max_floor)
                min_x_tag = min_floor_tag
                max_x_tag = max_floor_tag
                min_y_tag = min_floor_tag
                max_y_tag = max_floor_tag
                
            # If the current day/slot isn't good, advance to the next day/slot.  
            if not done:
                candidateDay += 1
                if candidateDay > days:
                    candidateDay = 1
                    candidateSlot += 1
                if candidateSlot >= timeSlotsPerDay:
                    candidateSlot = 1
    
    print('Done placing CPEs into Grid.')
            
    # Initialize some stats that we want to compute.
    max_cpes_in_slot_one_zone = 0
    sum_cpes_in_one_zone_in_one_slot = 0
    count_zone_slot = 0
    max_cpes = 0
    max_cpes_zone = []
    adjacent = 0
    
    # Generate a file with stats and other info that is computed to faciliate an analysis
    with open('results_{}_{}_{}.txt'.format(cluster_max_x, cluster_max_y, cluster_max_z), 'w') as out_data:   
        out_data.write('Configuration:\n')
        out_data.write('\tMax X = {}, Max Y = {}, Max Z = {}\n\n'.format(cluster_max_x, cluster_max_y, cluster_max_z))
        out_data.write('\tTime Slots Per Day = {}\n'.format(timeSlotsPerDay))
        out_data.write('\tDays in Cycle = {}\n'.format(days))
        out_data.write('\tMax attempts before picking any slot = {}\n'.format(max_attempts))
        out_data.write('\tNumber of attempts before relaxing zone limits = {}\n\n'.format(relax_attempts))
        out_data.write('Adjacent zone limits:\n')
        out_data.write('\tFloor Difference = {}, X Difference = {}, Y Difference = {}\n\n'.format(floor_difference,x_difference,y_difference))
        out_data.write('Number of CPEs = {}\n'.format(len(list_of_cpes)))
        zone_count = {}
        for an_cpe in list_of_cpes:
            if an_cpe['zoneTag'] in zone_count:
                zone_count[an_cpe['zoneTag']] += 1
            else:
                zone_count[an_cpe['zoneTag']] = 1
        for zone in zone_count:
            if zone_count[zone] > max_cpes:
                max_cpes = zone_count[zone]
                max_cpes_zone = []
                max_cpes_zone.append(zone)
            elif zone_count[zone] == max_cpes:
                max_cpes_zone.append(zone)
        out_data.write('Max CPEs in one zone = {}, Zone Tags = {}\n'.format(max_cpes, max_cpes_zone))
        out_data.write('Zone Layout of CPEs:\n')            
        for key, value in sorted(zone_count.items(), key=lambda x: x[0]): 
            out_data.write('\t{}:\t{}\n'.format(key, value))
        out_data.write('\n')
        out_data.write('Time Period Layout:\n')
        i = 1
        while i <= days:
            j = 1
            while j <= timeSlotsPerDay:
                theSlot = tSlotList.find_slot(i, j)
                zoneTagsInSlot = {}
                for cpeInSlot in theSlot['CPEs']:
                    if theSlot['CPEs'][cpeInSlot]['zoneTag'] not in zoneTagsInSlot:
                        zoneTagsInSlot[theSlot['CPEs'][cpeInSlot]['zoneTag']] = 1
                    else:
                        zoneTagsInSlot[theSlot['CPEs'][cpeInSlot]['zoneTag']] += 1
                orderedZoneTagsInSlot = sorted(zoneTagsInSlot.items())
                for zone in orderedZoneTagsInSlot:
                    sum_cpes_in_one_zone_in_one_slot += zone[1]
                    count_zone_slot += 1
                    if zone[1] > max_cpes_in_slot_one_zone:
                        max_cpes_in_slot_one_zone = zone[1]
                out_data.write('\tDay = {}, Time Slot = {}, # of CPEs = {}, Zones/CPEs In Slot = {}.\n'.format(i, j, len(theSlot['CPEs']), orderedZoneTagsInSlot))
                zonesOnly = []
                for entry in orderedZoneTagsInSlot:
                    zonesOnly.append(entry[0])
                for entry in orderedZoneTagsInSlot:
                    zone = entry[0]
                    zoneList = zone.split('_')
                    adjacentZones = ['Z_{}_{}_{}'.format(int(zoneList[1])-1, zoneList[2], zoneList[3]),
                                     'Z_{}_{}_{}'.format(int(zoneList[1])+1, zoneList[2], zoneList[3]),
                                     'Z_{}_{}_{}'.format(zoneList[1], int(zoneList[2])-1, zoneList[3]),
                                     'Z_{}_{}_{}'.format(zoneList[1], int(zoneList[2])+1, zoneList[3]),
                                     'Z_{}_{}_{}'.format(zoneList[1], zoneList[2], int(zoneList[3])-1),
                                     'Z_{}_{}_{}'.format(zoneList[1], zoneList[2], int(zoneList[3])+1)]
                    for adjZone in adjacentZones:
                        if adjZone in zonesOnly:
                            print('found adjacent match')
                            adjacent += 1
                j += 1
            i += 1
            
        out_data.write('\n')    
        out_data.write('Periodic Grid statistics:\n')
        out_data.write('\tMax CPEs in 1 zone = {}\n'.format(max_cpes_in_slot_one_zone))
        out_data.write('\tAvg CPEs per zone per slot = {}\n'.format(sum_cpes_in_one_zone_in_one_slot/count_zone_slot))      
        out_data.write('\tNumber of adjacent collisions = {}\n'.format(adjacent))
        out_data.write('\tNumber of times hit maximum attempts = {}\n'.format(got_to_max))
        out_data.write('\tNumber of placements attempts that required another attempt = {}\n'.format(times_look_again))
        out_data.write('\tNumber of times criteria relaxed = {}\n'.format(relaxed_criteria))
        
    print('Adjacent placements = {}'.format(adjacent))
    print('Number of times got to max = {}'.format(got_to_max))
    print('Number of times had to look again = {}'.format(times_look_again))
    module_end = time.time()
    print('Detla Time = {} seconds'.format(round(module_end-module_start,2)))

    
if __name__ == "__main__":
    main()

