"""
    Purpose: Script to demonstrate use of geoplotlib library.  Create a map that 
             shows the coronavirus cases through 03/22/2020.  Map is interactive, but slow.
    Input:   Two CSV files, one with the polygon dimensions of each county and 
             another with the number of coronavirsu cases by county through 03.22.2020.
             Source of raw data: https://usafacts.org/visualizations/coronavirus-covid-19-spread-map/
    Output:  None.
    Syntax:  python coronavirus.py
    Details: xTreem Analytics.  Copyright 2020 by J. Cartmell.
"""

import geoplotlib
import json
from geoplotlib.utils import BoundingBox
from geoplotlib.colors import ColorMap

# Convert the number of coronavirus cases per county into a color.
def get_color(properties):
    key = str(int(properties['STATE'])) + properties['COUNTY']
    if key in coronavirus:
        return cmap.to_color(coronavirus.get(key), 20, 'lin')
    else:
        return [0, 0, 0, 0]

# Get the number of cases for a specific county.
def get_cases(properties):
    key = str(int(properties['STATE'])) + properties['COUNTY']
    if key in coronavirus:
        return coronavirus[key]
    else:
        return 0

# Create a blank dictionary with the number of coronavirus cases by county.
# Note, if a county has none, then they are not in the list.
coronavirus = {}

# Create the map object.
cmap = ColorMap('Reds', alpha=255, levels=20)

def main():
    with open('coronavirus.csv') as f:
        # Open the CSV, read in each line and pull out the necessary information,
        # specifically, the state/county code and the number of cases.
        for cnt, line in enumerate(f):
            line = line.split(',')
            state_county = str(line[0])
            try:
                cases = int(line[4].replace('\n', ''))
                coronavirus[state_county] = cases
            except:
                pass

    # Populate the map object.
    geoplotlib.geojson('gz_2010_us_050_00_20m.json', fill=True, color=get_color, 
                       f_tooltip=lambda properties: '{}: {}'.format(properties['NAME'],
                       get_cases(properties)))
    geoplotlib.set_bbox(BoundingBox.USA)
    geoplotlib.show()

if __name__ == "__main__":
    main()

