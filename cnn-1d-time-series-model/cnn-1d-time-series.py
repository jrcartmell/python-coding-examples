import tensorflow as tf

class modelCallback(tf.keras.callbacks.Callback): 
    """
        Purpose: To allow for the model training to end when the desired accuracy is achieved/
        Input:   Accuracy Threshold
        Output   None
        Syntax modelCallback.{Method-ib-class}(self, accuracy_threshold)
    """

    def __init__(self, accuracy_threshold):
        self.accuracy_threshold = accuracy_threshold

    def on_epoch_end(self, epoch, logs={}): 
        if(logs.get('acc') > self.accuracy_threshold):   
            print('\nReached {}% accuracy, hence training terminated!'.format(round(self.accuracy_threshold*100,1))) 
            self.model.stop_training = True


class CNN1dModel:
    """
        Purpose: To demonstrate the use of the Keras Python library to build and train a 1D CNN
                 for time series data.
        Input:   Self and several parameters related to the configuration of the 1D CNN and the
                 splitting of data into training and test sets.
        Output:  None
        Syntax:  CNN1dModel.{Method-in-class}(self, test_percentage, verbose, epochs, batch_size,
                 kernel_size, no_filters, drop_out_rate, pool_size, no_features, no_timesteps
                 accuracy_threshold)
    """

    def __init__(self, test_percentage, verbose, epochs, batch_size, kernel_size, no_filters,
                 drop_out_rate, pool_size, no_features, no_timesteps, accuracy_threshold):
        # Store parameters to be used in this instance of the class.
        self.test_percentage = test_percentage  # The percentage of the data that is to be put into test set
        self.verbose = verbose              # Whether or not the Keras library should print details.
        self.epochs = epochs                # Number of epochs to iterate the model.
        self.batch_size = batch_size        # The number of samples to use at one time.
        self.kernel_size = kernel_size      # The number of features consiered at once.
        self.no_filters = no_filters        # Number of filters - number of features that can be detected at once.
        self.drop_out_rate = drop_out_rate  # Percentage of results to drop, helps prevent overfitting.
        self.pool_size = pool_size          # Pooling size value.
        self.no_features = no_features      # Number of features in the data.
        self.no_timesteps = no_timesteps    # Number of timesteps.
        self.accuracy_threshold = accuracy_threshold   # Required accuracy threshold where to stop learning.

    def format_data(self, data):
        """
            Purpose: To format the raw sample data and split it between training and test data.
            Input:   Data to be conditioned and split between training and test.
            Output:  Test data - features and classes and training data - features and classes.
            Syntax:  <Filter object>.format_data(data)
        """
    
        import numpy as np
        from sklearn.model_selection import train_test_split
        from keras.utils import to_categorical

        # Convert the raw CSV data to a numpy array.
        data = np.array(data[1:], dtype=np.float)

        # Split the data between the class (y) and the features (x)
        # The class is in the first column while the attributes are everything else.
        x = []
        y = []
        for q in range(0, len(data)):
            y.append(data[q][0])
            x.append(data[q][1:])

        # Convert to a numpy array.
        y = np.array(y)
        x = np.array(x)

        # Normalize the class by subtracting 1.
        y = y - 1
        
        # Convert the class column to a 2D structure.
        y = to_categorical(y)
      
        # Need to pad the dimensions of the feature data for the keras model.
        x = np.expand_dims(x, axis=2)

        # Split the data between training and test samples.
        try:
            self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(x, y, test_size=self.test_percentage)
        except Exception as e:
            print(e)

    def train_model(self, location):
        """
            Purpose: To train the 1D CNN model with the training data.
            Input:   None - The data is within the class instance.
            Output:  None - The data is within the class instance.
            Syntax:  <Filter object>.train_model()
        """

        from keras.models import Sequential
        from keras.layers import Dense
        from keras.layers import Flatten
        from keras.layers import Dropout
        from keras.layers import Reshape
        from keras import regularizers
        from keras.layers.convolutional import Conv1D
        from keras.layers.convolutional import MaxPooling1D
        from keras.layers import GlobalAveragePooling1D
        from keras.utils import np_utils

        # Extract the number of different classifications are possible.
        no_outputs = self.y_train.shape[1]
      
        callbacks = modelCallback(self.accuracy_threshold)

        # Instantiate an instance of the model.
        self.model = Sequential()
        
        # Build out the model, it is a 1D CNN. This can be augmented with more layers if needed.
        self.model.add(Reshape((self.no_timesteps, self.no_features), input_shape=(self.no_timesteps*self.no_features, 1)))
        self.model.add(Conv1D(filters=self.no_filters, kernel_size=self.kernel_size, activation='relu'))
        self.model.add(Conv1D(filters=self.no_filters, kernel_size=self.kernel_size, activation='relu'))
        self.model.add(MaxPooling1D(self.pool_size))
        self.model.add(Conv1D(filters=self.no_filters, kernel_size=self.kernel_size, activation='relu'))
        self.model.add(Conv1D(filters=self.no_filters, kernel_size=self.kernel_size, activation='relu'))
        self.model.add(Flatten())
        self.model.add(Dropout(self.drop_out_rate))
        self.model.add(Dense(no_outputs, activation='softmax'))

        self.model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        
        # Output a summary of the model.
        self.model.summary()
        
        # Develop the model with the training data.
        self.model.fit(self.x_train, self.y_train, epochs=self.epochs, batch_size=self.batch_size, verbose=self.verbose, callbacks=[callbacks])

        self.model.save(location)


    def eval_model(self):
        """
            Purpose: To evaluate the 1D CNN model with the training and test data.
            Input:   None - The data is within the class instance, so nothing needs to be passed in.
            Output:  The accuracy for the training data and test data
            Syntax:  <Filter object>.train_model()
        """
        
        # Evaluate the model using the training data.
        _, training_accuracy = self.model.evaluate(self.x_train, self.y_train, batch_size=self.batch_size, verbose=self.verbose)
        
        # Evaludate the model using the test data.
        _, test_accuracy = self.model.evaluate(self.x_test, self.y_test, 
                                               batch_size=self.batch_size, verbose=self.verbose)

        return training_accuracy, test_accuracy


def main():

    import csv

    do_curve_class = True
    model_name = 'curve_class_model'

    if do_curve_class:

        # Configure the parameters used in the CNN.
        test_percentage = 0.50  # Split the data into 50% for training, 50% for test.
        verbose = 1             # Turn on verbosity in the Keras library.
        epochs = 250            # Let the model iterate for 10 epochs.
        batch_size = 32         # Set the batch size to 32.
        kernel_size = 10        # Set the averaging window to 10.
        no_filters = 128        # Set the filter size to 64, to extract 64 features.
        drop_out_rate = 0.5     # Set the drop rate to 1/2, to prevent overfitting.
        pool_size = 3           # Set the pool size to 3 for the pooling layer.
        no_timesteps = 80       # Set the number of time samples to 80.
        no_features = 3         # There are three features in the data, one for each axis.
        accuracy_threshold = 0.98 # When to stop model training.

        # Instantiate the model.
        curve_class_model = CNN1dModel(test_percentage, verbose, epochs, batch_size, kernel_size,
                                       no_filters, drop_out_rate, pool_size, no_features,
                                       no_timesteps, accuracy_threshold)

        # Read in the Wine characteristics data.
        with open("curve_class.txt", 'r') as f:
            curve_class = list(csv.reader(f, delimiter=","))

        # Format the data, train the model and then evaluate it.
        curve_class_model.format_data(curve_class)
        curve_class_model.train_model(model_name)
        training_accuracy, test_accuracy = curve_class_model.eval_model()

        # Print out the accuracies.
        print('For Curve Class data:')
        print('    Model accuracy with training data is {}%'.format(100*round(training_accuracy,2)))
        print('    Model accuracy with test data is {}%'.format(100*round(test_accuracy,2)))
        print('    Model saved as: {}'.format(model_name))


if __name__ == "__main__":
    main()

