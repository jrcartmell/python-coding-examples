# Some simple code that will create classification time series data for
# as training/test data for a 1d CNN.
import csv
from random import randint

# Define a function that will add random noise to a value.
def randy(x):
    return round((x+(200-randint(1,400))/50),3)

# Open the file where the random class data is to be stored.
with open('curve_class.txt', mode='w') as csv_file:
    
    the_writer = csv.writer(csv_file, delimiter=',')
    the_writer.writerow(["Class","Data1","Data2","Data3"])

    # Set the number of data samples for each feature.
    number_of_points = 80

    # Set the number of data samples.
    number_of_samples = 2000

    # Create a few different patterns.
    pattern_a = [10, 20, 30, 20]
    pattern_b = [30, 20, 10, 20]
    len_pattern_a = len(pattern_a)
    len_pattern_b = len(pattern_b)

    # Create the data itself.  Randomly add noise to the base patterns.
    # Each data sample has 3 features, each feature with number_of_points
    # samples - these are time series.
    for j in range(1, number_of_samples):
        
        pattern_1 = []
        pattern_2 = []
        pattern_3 = []
        pattern_4 = []
        pattern_5 = []
        for i in range(0, number_of_points):
            pattern_1.append(round(randy(i+1)/number_of_points,3))
            pattern_2.append(round(randy(0),3))
            pattern_3.append(round(randy(pattern_a[i%len_pattern_a])/30,3))
            pattern_4.append(round(randy(number_of_points-i)/number_of_points,3))
            pattern_5.append(round(randy(pattern_b[i%len_pattern_b])/30,3))


        # Add the data to the CSV - both the classification and the sample
        # data to be classified.
        the_writer.writerow([1] + pattern_1 + pattern_2 + pattern_3)
        the_writer.writerow([2] + pattern_2 + pattern_3 + pattern_4)
        the_writer.writerow([3] + pattern_3 + pattern_4 + pattern_5)
        the_writer.writerow([4] + pattern_4 + pattern_5 + pattern_1)
        the_writer.writerow([5] + pattern_5 + pattern_1 + pattern_2)
