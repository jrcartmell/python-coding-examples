#!/usr/bin/env python3
"""
    Purpose: To demonstrate the use of a dictionary to compare two
             lists of integers and display the items that are in
             both.
    Syntax:  python hash.py
    Use: Free use, no licensing required.
    Details: xTreemAnalytics.  Copyright 2021 by J. Cartmell.
"""
import datetime
import random

# Create the first list, starting with the value 1 and then adding
# 9999 random integers.
x = [1]
val = 1
for i in range(9999):
  new_val = val +  random.randrange(1, 10)
  val = new_val
  x.append(val)

# Create the second list, starting with the value 1, and then adding
# 999 random integers
y = [1]
val = 1
for i in range(9999):
  new_val = val +  random.randrange(1, 10)
  val = new_val
  y.append(val)

print("Compare between using hash and brute force to find occurrences of integers in a list with another list.")
print("Each list has {} items".format(len(x)))
print("Each list has the value '1' so that there is at least one match.")

start_time = datetime.datetime.now()
# Create a dictionary with the 'hash' values.  The hash values
# are just the index value.  So the hash value of 1 is 1.
dict = {}
for item in x:
    dict[str(item)] = item

# Check to see if the items in y are in the hashed dictionary.    
matches = []
for item in y:
    try:
        matches.append(dict[str(item)])
    except:
        pass
end_time = datetime.datetime.now()
delta_time = end_time-start_time
print("Time for using dictionary = {} and resulted in {} matches.".format(delta_time, len(matches)))
        
start_time = datetime.datetime.now()
# Implement the brute force technique of comparing every
# item in the 2nd list against every item in the 1st list.
matches = []
for i in x:
    for j in y:
        if i==j:
            matches.append(i)
end_time = datetime.datetime.now()
delta_time = end_time-start_time
print("Time for using dictionary = {} and resulted in {} matches.".format(delta_time, len(matches)))

