#!/usr/bin/env python3
"""
    Purpose: To demonstrate a Wilson confidence interval from a
             population samples.
    Syntax:  python conf_interval.py
    Use: Free use, no licensing required.
    Details: xTreemAnalytics.  Copyright 2021 by J. Cartmell.
"""
import math
import sys

# Mapper of confidence values to z values
z_value = {'90': 1.645, '95': 1.96, '99': 2.576}

# Get user input for pop size, positives and desired conf interval.
sample_size = input("Sample Size: ")
positives = input("Positives: ")
desired_conf_int = input("Desired Confidence Interval (Only enter 90, 95 or 99): ")

# Convert the sample size to integer, throw exception if fails.
try:
    sample_size = int(sample_size)
except:
    print("Sample size does not seem to be an integer.")
    sys.exit(1)

# Convert the positives to integer, throw exception if fails.    
try:
    positives = int(positives)
except:
    print("Positives does not seem to be an integer.")
    sys.exit(1)

# Mapper the desired conf interval to a z value, throw exception if fails.
try:
    z = z_value[desired_conf_int]
except:
    print("Something is amiss with the confidence interval.")
    sys.exit(1)

# If positives greater than population, there is an error, note it and exit.
if positives > sample_size:
    print("Population is smaller than positives, which is not feasible.")
    sys.exit(1)
    
# Compute the confidence interval using the Wilson equation, throw exception if fails
try:
    # Calc positive sample rate
    sample_prob = positives/sample_size

    # Compute Wilson equation components
    z_sq = z**2
    part_a = (1/(1+z_sq/sample_size)) * (sample_prob+z_sq/sample_size/2)
    part_b = (z/(1+z_sq/sample_size)) * math.sqrt((sample_prob*(1-sample_prob)/sample_size)+
                                                  (z_sq/4/sample_size**2))

    # Print the results.
    print("/n/n")
    print("Samples: {}".format(sample_size))
    print("Positives: {}".format(positives))
    print("Population positive: {}".format(round(positives/sample_size,4)))
    print("For a confidence interval of {}%".format(desired_conf_int))
    print("Lower bound: {}".format(round(part_a-part_b, 4)))
    print("Upper bound: {}".format(round(part_a+part_b, 4)))

except ZeroDivisionError:
    print("Dividing by zero")
except Exception as e:
    print("Uh-oh, got this error: {}".format(e))
    
