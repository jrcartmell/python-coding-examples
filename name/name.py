class Name(object):
    """
        Purpose: Class to demonstrate use of classmethod and staticmethod decorators with 
                 really simple class.  Note, you wouldn't do this in real coding, this is
                 just an example.
        Syntax:  Name.{Method-in-class}
        Details: XCellAir, Inc.  Copyright 2020 by J. Cartmell.
    """

    def __init__(self, first_name, last_name):
        """
            Purpose: The init function of the class.  When invoked, store the first and last names.
            Input:   first_name, last_name
            Output:  None
            Syntax:  Name(first_name, last_name)
        """        
        # If an object of the class is instantiated as follows x = Name('first', 'second'), this method is invoked.
        # If an object of the class is instantiated via the classmethod x = Name.split_name('Al Ott'), this method is invoked.
        print('    In Name class __init__ method.')
        self.first_name = first_name
        self.last_name = last_name

    def get_full_name(self):
        """
            Purpose: Returns the full name from the instantiated class object.
            Input:   None
            Output:  Full name
            Syntax:  get_full_name()
        """        
        # This can only be invoked from an instantiated class object.
        print('    In Name class, get_full_name method.')
        return self.first_name + ' ' + self.last_name

    @classmethod
    def split_name(cls, full_name):
        """
            Purpose: Creates an instance of the class, uses full name as input to create object with
                     first and last name.
            Input:   Full Name
            Output:  Name
            Syntax:  split_name(full_name)
        """        
        # Since this is defined as a class method, if it is invoked it creates an object of the class.
        print('    In Name class, split_name class method.')
        first_name, last_name = map(str, full_name.split(' '))
        name = cls(first_name, last_name)
        return name
        
    @staticmethod
    def check_if_full_name(name):
        # This method can be invoked with just a name and it will check if it is a full name.  No class object needed.
        print('    In Name class, check_if_full_name static method.')
        split_name = name.split(' ')
        return len(split_name) > 1        
        

print('Demonstrates class instances using classmethod and staticmethod.')
print('')

# Create instance of the class using the class name.
print('**** Player 1 instance ****')
player1 = Name('Ted',  'Martinez')
# Access some of the methods of this instance.
print('  Player 1 first name = {}'.format(player1.first_name))
print('  Player 1 full name = {}'.format(player1.get_full_name()))

print('')
# Create instance of the class by invoking the classmethod split_name.
print('**** Player 2 instance ****')
player2 = Name.split_name('Duffy Dyer')
print('  Player 2 first name = {}'.format(player2.first_name))
print('  Player 2 full name = {}'.format(player2.get_full_name()))

print('')
print('**** Player 3 instance ****')
# Use the static method check_if_full_name to determine if a namehas both a first and last name.
# Notice, this doesn't really create an instance of the class.
player3 = 'Don Hahn'
print('  Player 3 full name = {}'.format(player3))
print('  Player 3 name is a full name? {}'.format(Name.check_if_full_name(player3)))