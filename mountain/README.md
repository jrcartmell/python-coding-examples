# Mountain Number Validator

## Mountain Number Definition
This repo provides a class which will expose a method that evaluates whether an input is a mountain number.

What is a mountain number you ask?  It is a number that meets the following criteria:
1. The number must start and end at the same height - 12321 is valid but 123432 is not.
2. The number has a single 'peak' digit - '5' is the peak for 134521.
3. It does not have to start (and end) at 1 - 236862 is valid.
4. It does not contain any local peaks - the digits before the ultimate peak are always increasing and after the peak are always decreasing - 1324311 is not a valid mountain as 2<3 and 1=1.
5. It also must be longer than 2 digits and, obviously, must be an integer.

## File Structure
The following files are in this repo:
 - mountain.py - Defines the Mountain class which includes the validate_mountain_list method for issuing a verdict for each number if it is or is not a Mountain number.
 - main.py - Creates an instance of the Mountain class, accepts user input, invokes the validate_mountain_list method with the user input and then displays the result to the user.
 - test.py - Uses unittest to test the Mountain class for a number of possible inputs.
 - requirements.txt - Lists the Python packages required.

## Usage
There are several steps that must be followed.  Since this was tested on a Linux box, a Conda environment was created with Python 3.9.  As such, Conda needs to be installed.  The version used for this exercise is Conda 4.10.1.  Please use that version or later.

At the command prompt enter: 
```bash
conda create -n "py39" python=3.9
```
to create a Conda environment for Python 3.9.  Answer yes to all questions during the install.

Activate the conda environment, enter at the command prompt:
```bash
conda activate py39
```
Check the Python version is greater than 3.9, enter at the command prompt: 
```bash
python3 --version
```
It should be 3.9.5 or thereabouts.

To run the unit test, at the command prompt enter:
```bash
python3 test.py
```
You should see that the testing passed.

To run the main program, at the command prompt enter:
```bash
python3 main.py
```
and follow the prompts on the screen.