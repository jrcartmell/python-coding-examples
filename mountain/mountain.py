class Mountain:
    '''
        Purpose: A class which exposes a method which will test whether a
                 number, or a list of numbers, is/are a mountain number or
                 not.
        Details: xTreem Analytics, Inc.  Copyright 2021 by J. Cartmell.
    '''
    def validate_mountain_list(self, the_input):
        '''
            Purpose: A method which will test whether a number, or a list
                     of numbers, is/are a mountain number or not.  A Mountain
                     number must meet this criteria:
                     1. The number must start and end at the same height - 
                        12321 is valid but 123432 is not.
                     2. The number has a single 'peak' digit - '5' is the
                        peak for 134521.
                     3. It does not have to start (and end) at 1 - 236862 is 
                         valid.
                     4. It does not contain any local peaks - the digits
                         before the ultimate peak are always increasing and 
                         after the peak are always decreasing - 1324311 is not
                         a valid mountain as 2<3 and 1=1.
                     5. It also must be longer than 2 digits and, obviously, 
                        must be an integer.
            Input:   Comma delimited sequence of numbers to validate, 
            Output:  List of verdicts, true/false order corresponds to the 
                     order of the input list. 
            Syntax:  <Mountain>.validate_mountain_list(list of numbers
                     delimited by comma)
        '''
        # Handle cases where the input is a single number or a list
        # delimited with commas.
        if type(the_input) is list:
            the_list = the_input
        elif type(the_input) is str:
            the_input = the_input.replace(" ", "")
            the_list = the_input.split(",")

        # Create an empty list with the verdict for each value
        the_answer = []

        # Iterate through each value to determine if it is a mountain number.
        # Once a determination is made, append either true or false to the
        # return list.
        for the_item in the_list:
            if len(the_item) < 3 or the_item[0] != the_item[-1]:
                the_answer.append(False)
            else:
                i = 1
                while i < len(the_item) and the_item[i] > the_item[i-1]:
                    i += 1
                if i == 1 or i == len(the_item):
                    the_answer.append(False)
                else:
                    while i < len(the_item) and the_item[i] < the_item[i-1]:
                        i += 1
                    the_answer.append(i == len(the_item))

        return the_answer
