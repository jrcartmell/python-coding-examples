from mountain import Mountain

if __name__ == '__main__':
    '''
        Purpose: To create an instance of the Mountain class, query
                 the user for a list of numbers to validate, invoke
                 a method in the class to validate the numbers, and
                 display the verdict to the user.
        Input:   None
        Output:  None
        Syntax:  python main.py
        Details: xTreem Analytics, Inc.  Copyright 2021 by J. Cartmell.
    '''

    # Create an instance of the Mountain class, providing us with the
    # validate_mountain_list method.
    mountain_validator = Mountain()

    # Query the user to enter a number or list of numbers.
    input_to_check = input('Enter numbers to check separated by commas: ')

    # Invoke the method in the Mountain class to yield a verdict.
    verdict = mountain_validator.validate_mountain_list(input_to_check)

    # Display the verdict to the user.
    print('For input: {}; the results are: {}'.format(input_to_check, verdict))
