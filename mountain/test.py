import unittest

from mountain import Mountain


class TestMountain(unittest.TestCase):
    '''
        Purpose: To test the validate_mountain_list method in the
                 Mountain class and display whether the tests passed
                 or failed.
        Input:   None
        Output:  None
        Details: xTreem Analytics, Inc.  Copyright 2021 by J. Cartmell
    '''
    def test_mountain_list(self):
        '''
            Purpose: Test that Mountain class correctly identifies numbers
                     which meet certain criteria as mountain numbers and
                     correctly classifies numbers which do not meet the
                     criteria as not mountain numbers.
            Input:   None
            Output:  None
        '''

        # Should fail since first and last does not match.
        verdict = Mountain.validate_mountain_list(self, '0321')
        self.assertEqual(verdict, [False])

        # Should fail, fail and pass.
        verdict = Mountain.validate_mountain_list(self, '1234,0321, 14541')
        self.assertEqual(verdict, [False, False, True])

        # Should fail since it is not a number.
        verdict = Mountain.validate_mountain_list(self, 'hello')
        self.assertEqual(verdict, [False])

        # Should fail since it has more than one peak.
        verdict = Mountain.validate_mountain_list(self, '34579876543')
        self.assertEqual(verdict, [True])

        # Should fail since it is only 1 number
        verdict = Mountain.validate_mountain_list(self, '5')
        self.assertEqual(verdict, [False])


if __name__ == '__main__':
    '''
        Purpose: To test the Mountain class with a variety of different
                 types of numbers and even some non-numbers to test the
                 implementation.
        Input:   None
        Output:  None
        Syntax:  python test.py
        Details: xTreem Analytics, Inc.  Copyright 2021 by J. Cartmell.
    '''

    unittest.main()
