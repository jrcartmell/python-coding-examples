class CNN1dModel:
    """
        Purpose: To demonstrate the use of the Keras Python library to build and train a 1D CNN.
        Input:   Self and several parameters related to the configuration of the 1D CNN and the
                 splitting of data into training and test sets.
        Output:  None
        Syntax:  CNN1dModel.{Method-in-class}(self, test_percentage, verbose, epochs, batch_size, kernel_size, no_filters, no_layers, drop_out_rate)
    """

    def __init__(self, test_percentage, verbose, epochs, batch_size, kernel_size, no_filters, no_layers, drop_out_rate):
        # Store parameters to be used in this instance of the class.
        self.test_percentage = test_percentage  # The percentage of the data that is to be put into test set
        self.verbose = verbose              # Whether or not the Keras library should print details.
        self.epochs = epochs                # Number of epochs to iterate the model.
        self.batch_size = batch_size        # The number of samples to use at one time.
        self.kernel_size = kernel_size      # The number of features consiered at once.
        self.no_filters = no_filters        # Number of filters - number of features that can be detected at once.
        self.no_layers = no_layers          # Number of hidden layers in the 1D CNN.
        self.drop_out_rate = drop_out_rate  # Percentage of results to drop, helps prevent overfitting.
    
    def format_data(self, data):
        """
            Purpose: To format the raw sample data and split it between training and test data.
            Input:   Data to be conditioned and split between training and test.
            Output:  Test data - features and classes and training data - features and classes.
            Syntax:  <Filter object>.format_data(data)
        """
    
        import numpy as np
        from sklearn.model_selection import train_test_split
        from keras.utils import to_categorical
        
        # Convert the raw CSV data to a numpy array.
        data = np.array(data[1:], dtype=np.float)
        
        # The class of each sample is in the first column of the array.
        y = data[:,0]
        
        # The features of each sample are in the rest of the columns.
        x = data[:,1:]
        
        # Normalize the attribute data
        x_mean = np.mean(x, axis=(0))
        x_std = np.std(x, axis=(0))
        x = (x - x_mean)/(x_std + 1e-07)
        
        # Normalize the class by subtracting 1.
        y = y - 1
        
        # Convert the class column to a 2D structure.
        y = to_categorical(y)
        
        # Need to pad the dimensions of the feature data for the keras model.
        x = np.expand_dims(x, axis=2)
        
        # Split the data between training and test samples.
        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(x, y, test_size=self.test_percentage)

    
    def train_model(self):
        """
            Purpose: To train the 1D CNN model with the training data.
            Input:   None - The data is within the class instance, so nothing needs to be passed in.
            Output:  None - The data is within the class instance, so nothing needs to be passed out.
            Syntax:  <Filter object>.train_model()
        """

        from keras.models import Sequential
        from keras.layers import Dense
        from keras.layers import Flatten
        from keras.layers import Dropout
        from keras import regularizers
        from keras.layers.convolutional import Conv1D
        from keras.layers.convolutional import MaxPooling1D
        from keras.utils import np_utils

        # Set a number of values used in the model based on the input data shape.
        n_timesteps = self.x_train.shape[1]
        n_features = self.x_train.shape[2]
        n_outputs = self.y_train.shape[1]
        
        # Instantiate an instance of the model.
        self.model = Sequential()
        
        # Build out the model, it is a 1D CNN.
        self.model.add(Conv1D(filters=self.no_filters, kernel_size=self.kernel_size, activation='relu', input_shape=(n_timesteps,n_features)))
        for i in range(2, self.no_layers):
            self.model.add(Conv1D(filters=self.no_filters, kernel_size=self.kernel_size, activation='relu'))
        self.model.add(Dropout(self.drop_out_rate))
        self.model.add(MaxPooling1D(pool_size=2))
        self.model.add(Flatten())
        self.model.add(Dense(100, activation='relu'))
        self.model.add(Dense(n_outputs, activation='softmax'))
        self.model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        
        # Output a summary of the model.
        self.model.summary()
        
        # Develop the model with the training data.
        self.model.fit(self.x_train, self.y_train, epochs=self.epochs, batch_size=self.batch_size, verbose=self.verbose)


    def eval_model(self):
        """
            Purpose: To evaluate the 1D CNN model with the training and test data.
            Input:   None - The data is within the class instance, so nothing needs to be passed in.
            Output:  The accuracy for the training data and test data
            Syntax:  <Filter object>.train_model()
        """
        
        # Evaluate the model using the training data.
        _, training_accuracy = self.model.evaluate(self.x_train, self.y_train, batch_size=self.batch_size, verbose=self.verbose)
        
        # Evaludate the model using the test data.
        _, test_accuracy = self.model.evaluate(self.x_test, self.y_test, batch_size=self.batch_size, verbose=self.verbose)

        return training_accuracy, test_accuracy


def main():

    import csv


    do_wines = True
    do_covtypes = False
    
    if do_wines:
        # Set the configuration parameters for the 1D CNN
        test_percentage = 0.15  # Split the data into 85% for training, 15% for test
        verbose = 0             # Turn off verbosity in the Keras library
        epochs = 250            # Let the model iterate for 250 epochs
        batch_size = 32         # Set the batch size to 32.
        kernel_size = 3         # Set the averaging window to 3.
        no_filters = 64         # Set the filter size to 64, to extract 64 features.
        no_layers = 4           # Set the number of layers to 4.
        drop_out_rate = 0.5     # Set the drop rate to 1/2, to prevent overfitting.

        # Instantiate the model.
        wine_model = CNN1dModel(test_percentage, verbose, epochs, batch_size, kernel_size, no_filters, no_layers, drop_out_rate)
        
        # Read in the Wine characteristics data.
        with open("wine.txt", 'r') as f:
            wines = list(csv.reader(f, delimiter=","))
    
        # Format the data, train the model and then evaluate it.
        wine_model.format_data(wines)
        wine_model.train_model()
        training_accuracy, test_accuracy = wine_model.eval_model()
    
        # Print out the accuracies.
        print('For Wine data:')
        print('    Model accuracy with training data is {}%'.format(100*round(training_accuracy,2)))
        print('    Model accuracy with test data is {}%'.format(100*round(test_accuracy,2)))


    if do_covtypes:
        # Set the configuration parameters for the 1D CNN
        test_percentage = 0.15  # Split the data into 85% for training, 15% for test
        verbose = 0             # Turn off verbosity in the Keras library
        epochs = 250            # Let the model iterate for 250 epochs
        batch_size = 32         # Set the batch size to 32.
        kernel_size = 3         # Set the averaging window to 3.
        no_filters = 256        # Set the filter size to 256, to extract 256 features.
        no_layers = 6           # Set the number of layers to 6.
        drop_out_rate = 0.5     # Set the drop rate to 1/2, to prevent overfitting.

        # Instantiate the model.
        covtype_model = CNN1dModel(test_percentage, verbose, epochs, batch_size, kernel_size, no_filters, no_layers, drop_out_rate)
        
        # Read in the Wine characteristics data.
        with open("covtype.txt", 'r') as f:
            covtypes = list(csv.reader(f, delimiter=","))

        # Format the data, train the model and then evaluate it.
        covtype_model.format_data(covtypes)
        covtype_model.train_model()
        training_accuracy, test_accuracy = covtype_model.eval_model()
    
        # Print out the accuracies.
        print('For Covtype data:')
        print('    Model accuracy with training data is {}%'.format(100*round(training_accuracy,2)))
        print('    Model accuracy with test data is {}%'.format(100*round(test_accuracy,2)))


if __name__ == "__main__":
    main()

