# This script demostrates the use of Keras library to build, train, test,
# evaluate and save a 2D Convolutional Neural Network.  It can be extended to
# do deep learning with many, many layers.  Currently there are only two.
# Also, the number of epochs is hardcoded to 3.  This can also be extended for
# deeper learning.

import keras
from keras.datasets import mnist
from keras import layers
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from termcolor import colored

# Define a helper function that prints put the shape of the training 
# test data - both the attributes and the class.
def show_data_shapes(x_train, y_train, x_test, y_test):
    print('Training shape:')
    print('\tx_train.shape: {}'.format(x_train.shape))
    print('\ty_train.shape: {}'.format(y_train.shape))
    print('\nTesting shape:')
    print('\tx_test.shape: {}'.format(x_test.shape))
    print('\ty_test.shape: {}'.format(y_test.shape))

# mnist is a data set of 28x28 grayscale images of digts 0 through 9.  Each sample 
# has the image and its classification.  There are 60,000 training samples and
# 10,000 test samples.  The mnist method to load the data returns two tuples.
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Get the number of rows and columns in the data.
img_rows = x_train.shape[1]
img_cols = x_train.shape[2]

# Since this is grayscale data there is no RGB component like 3D color images.
# This reshapes the data since the keras data sets can be channels first or 
# channel last.  This data set is channels last.
x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
input_shape = (img_rows, img_cols, 1)

# Normalize the data.  Instead of each pixel intensity being from 0 to 255,
# normalize each intensity by dividing by 255.  Also cast the data as float
# rather than integer, since now all values will be between 0 and 1.
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

# Print out shape of the attributes, classes - both training and test data.
show_data_shapes(x_train, y_train, x_test, y_test)

#set number of categories
num_category = 10

# Convert class values to binary class matrix.  
y_train = keras.utils.to_categorical(y_train, num_category)
y_test = keras.utils.to_categorical(y_test, num_category)

# Start building the model/
model = Sequential()

# Add a convolutional layer with 32 filters applied to each 3 by 3 square.
model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=input_shape))

# Add a convolutional layer with 64 filters applied to each 3 by 3 output of the previous layer.
model.add(Conv2D(64, (3, 3), activation='relu'))

# Add a pooling layer to extract out the most prominent features
model.add(MaxPooling2D(pool_size=(2, 2)))

# Randomly drop some data to prevent overfitting.
model.add(Dropout(0.3))

# Flatten the output of the previously layer
model.add(Flatten())

# Fully connect the output of the previous layer
model.add(Dense(128, activation='relu'))

# Randomly drop some more data to further prevent overfitting.
model.add(Dropout(0.4))

# Convert the output to classes probabilities.
model.add(Dense(num_category, activation='softmax'))

# Compile the model.
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
                           
# Fit the model to the training data.
model.fit(x_train, y_train, batch_size=128, epochs=3, verbose=1)

# Evaluate the model against the training data.          
train_loss, train_accuracy = model.evaluate(x_train, y_train, verbose=0)
print('\nModel results against training data:')
print('\tTest loss: {}'.format(round(train_loss, 2)))
print('\tTest accuracy: {}'.format(round(train_accuracy, 2)))

test_loss, test_accuracy = model.evaluate(x_test, y_test, verbose=0)
print('\nModel results against test data:')
print('\tTest loss: {}'.format(round(test_loss, 2)))
print('\tTest accuracy: {}'.format(round(test_accuracy, 2)))

# Save the model after serializing the model to JSON.
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)

# Serialize weights to HDF5.
model.save_weights("model.h5")

