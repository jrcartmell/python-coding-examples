#!/usr/bin/python3
"""
    Purpose: To load the OUI Mapping text file from IEEE into a CSV that can be used.
    Input:   None
    Output:  None
    Syntax:  python3 oui_mapping.py
    Details: XTreem Analytics  Copyright 2020 by J. Cartmell.
"""

################################################################################################
# Source of text file is here:  http://standards-oui.ieee.org/oui.txt                          #
# A typical entry in this raw file is as follows:                                              #
#   00-00-00   (hex)		    XEROX CORPORATION                                              #
#   000000     (base 16)		XEROX CORPORATION                                              #
#                               M/S 105-50C                                                    #
#                               WEBSTER  NY  14580                                             #
#                               US                                                             #
#                                                                                              #
# The below code will only process the (base 16) line:                                         #
#   000000     (base 16)		XEROX CORPORATION                                              #
#                                                                                              #
# and it will insert a record into CSV file with the following information:                    #
#   'Prefix'   'Manufacturer'                                                                  #
#   '000000'   'Xerox Corporation'                                                             #
#                                                                                              #
################################################################################################

import string
import urllib.request

# Method to clean up the text entries in the source text file.
def cleanup_entry(entry):
    entry = entry.replace('     ','  ')
    entry = entry.replace('   ', '  ')
    entry = entry.replace('(hex)', '')
    entry = entry.replace('(base 16)', '')
    entry = entry.replace('\r\n', '')
    entry = entry.replace('\n', '')
    entry = entry.replace('\t', '')
    entry = entry.split('  ')
    
    return entry

def main():
    # Set the URL where you can get the O.U.I. text file.
    url = 'http://standards-oui.ieee.org/oui.txt'

    list_of_params = ['Prefix', 'Manu']

    # This is the CSV file header.  These are the field names.  We delimit this file by semi-colon since some people use "," in their SSID names.
    header = 'Prefix,Manufacturer'

    # Create the CSV file.
    file_name = 'oui-mapping.txt'
    f = open(file_name, 'w+', encoding='utf-8')
    f.write('{}\n'.format(header))

    # Loop through the oui mapping file and try to insert each entry into Mongo.
    valid_mappings = 0
    with urllib.request.urlopen(url) as the_file:
        # For each entry, clean up the names/format and write the entry into the CSV file.
        for line in the_file:
            entry = line.decode('latin-1')
            if 'base 16' in entry:
                entry = cleanup_entry(entry)
                try:
                    # Only try to write the record if the prefix is a valid hex string, like '000000' or 'abcdef'.
                    # Otherwise it is not valid and don't pollute the CSV with it.
                    if all(c in string.hexdigits for c in entry[0]):
                        plain_entry = {'Prefix': entry[0], 'Manu': entry[1].title()}
                        valid_mappings += 1
                        for param in list_of_params:
                            try:
                                f.write('{},'.format(plain_entry[param]))
                            except:
                                pass
                        f.write("\n")
                except:
                    pass

    f.close()
    
    print('Found {} valid mappings, all written to CSV, fini.'.format(valid_mappings))

if __name__ == "__main__":
    main()

