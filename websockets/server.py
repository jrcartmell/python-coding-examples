import asyncio
import configparser
import logging
import websockets

class Server:
    clients = set()
  
  
    async def register(self, theWebSocket: websockets.WebSocketServerProtocol):
        self.clients.add(theWebSocket)
        logger.info('{} connects.'.format(theWebSocket.remote_address))


    async def unregister(self, theWebSocket: websockets.WebSocketServerProtocol):
        self.clients.remove(theWebSocket)
        logger.info('{} disconnects.'.format(theWebSocket.remote_address))


    async def send_to_clients(self, message: str):
        if self.clients:
            await asyncio.wait([aClient.send(message) for aClient in self.clients])


    async def ws_handler(self, theWebSocket: websockets.WebSocketServerProtocol, url: str):
        await self.register(theWebSocket)
        try:
            await self.distribute(theWebSocket)
        finally:
            await self.unregister(theWebSocket)

    
    async def distribute(self, theWebSocket: websockets.WebSocketServerProtocol):
        async for message in theWebSocket:
            await self.send_to_clients(message)


def main():
    config = configparser.ConfigParser()
    config.read('config.ini')
    port = config.getint('CONFIG', 'port')

    logger = logging.getLogger("main")
    logger.setLevel(logging.INFO)      # This can be changed as needed.
    file_handler = logging.FileHandler(config.get('LOGGING', 'server_log'))
    log_format = logging.Formatter('%(levelname)s - %(asctime)s - %(name)s - %(message)s')
    file_handler.setFormatter(log_format)
    logger.addHandler(file_handler)
    
    logger.info('*** Start ***')

    server = Server()
    theServer = websockets.serve(server.ws_handler, host='', port=port)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(theServer)
    loop.run_forever()    


if __name__ == "__main__":
    main()
    