import asyncio
import configparser
import logging
import websockets


async def consumer_handler(websocket: websockets.WebSocketClientProtocol):
    async for message in websocket:
        log_message(message)
        do_something(message)


async def consume(host: str, port: int):
    webSocketResourceUrl = 'ws://{}:{}'.format(host, port)
    async with websockets.connect(webSocketResourceUrl) as theWebSocket:
        await consumer_handler(theWebSocket)


def do_something(message: str):
    # Do something with the message received.
    pass

    
def log_message(message: str):
    logger.info('Message consumed: {}'.format(message))

    
def main():

    config = configparser.ConfigParser()
    config.read('config.ini')
    host = config.get('CONFIG', 'host')
    port = config.getint('CONFIG', 'port')

    logger = logging.getLogger("main")
    logger.setLevel(logging.INFO)      # This can be changed as needed.
    file_handler = logging.FileHandler(config.get('LOGGING', 'consumer_log'))
    log_format = logging.Formatter('%(levelname)s - %(asctime)s - %(name)s - %(message)s')
    file_handler.setFormatter(log_format)
    logger.addHandler(file_handler)
    
    logger.info('*** Start ***')

    loop = asyncio.get_event_loop()
    loop.run_until_complete(consume(host=host, port=port))
    loop.run_forever()


if __name__ == "__main__":
    main()
