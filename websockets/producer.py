import asyncio
import configparser
import logging
import websockets


async def produce(message: str, host: str, port: int):
    async with websockets.connect('ws://{}:{}'.format(host, port)) as theWebSocket:
        await theWebSocket.send(message)
        await theWebSocket.recv()
  
  
def main():
    config = configparser.ConfigParser()
    config.read('config.ini')
    host = config.get('CONFIG', 'host')
    port = config.getint('CONFIG', 'port')

    logger = logging.getLogger("main")
    logger.setLevel(logging.INFO)      # This can be changed as needed.
    file_handler = logging.FileHandler(config.get('LOGGING', 'producer_log'))
    log_format = logging.Formatter('%(levelname)s - %(asctime)s - %(name)s - %(message)s')
    file_handler.setFormatter(log_format)
    logger.addHandler(file_handler)
    
    logger.info('*** Start ***')

    loop = asyncio.get_event_loop()
    x = 0
    while (x < 100):
        message = 'Hello, this is message # {}'.format(x)
        logger.info('Message produced: {}'.format(message))
        loop.run_until_complete(produce(message=message, host=host, port=port))
        x = x + 1


if __name__ == "__main__":
    main()
 