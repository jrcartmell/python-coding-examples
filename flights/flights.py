"""
    Purpose: Demonstrate geoplotlib use for tracking position of objects. 
    Input:   None
    Output:  None
    Syntax:  python flights.py
    Details: xTreem Analytics.  Copyright 2020 by J. Cartmell.
"""

import geoplotlib
import pandas as pd
import numpy as np

# Set the number of points, rows and columns.  4 columns - start lat and long and end lat and long
# and 2500 rows.
cols = 4
rows = 2500

# use NumPy to generate random positions and then adjust so they are centered over North America.
random_positions = np.random.normal(loc=40, scale=4, size=rows*cols).reshape(rows, cols)
random_positions[:,1] = random_positions[:,1] - 140
random_positions[:,3] = random_positions[:,3] - 140

# These are the names of the columns.  These could be called anything, as long as they are used
# in the graph command below.
names = ['lat_departure', 'lon_departure', 'lat_arrival', 'lon_arrival']

# Convert the data to a Pandas dataframe and write out to a CSV.
data_frame = pd.DataFrame(random_positions, columns=names)
data_frame.to_csv('positions.csv', index=False, header=True, sep=',')

# Use the geoplotlib method to read in the positions.
data = geoplotlib.utils.read_csv('positions.csv')

# Create the image.
geoplotlib.graph(data,
                 src_lat='lat_departure',
                 src_lon='lon_departure',
                 dest_lat='lat_arrival',
                 dest_lon='lon_arrival',
                 color='Blues',
                 alpha=16,
                 linewidth=2)
geoplotlib.show()