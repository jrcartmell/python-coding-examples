class Filter:
    """
        Purpose: To demonstrate the use of the Python filter feature, applying it to a list of items.
        Input:   Self, logger
        Output:  None
        Syntax:  Filter.{Method-in-class}(self, logger)
    """
    def __init__(self, logger):
        # Store logger to be used in this instance of the class.
        self.logger = logger

    def test(item):
        """
            Purpose: To test if an item is a string, if so, return true, otherwise return false
            Input:   Item - To be determined if string or not
            Output:  True (if a string), False (Otherwise)
            Syntax:  <Filter object>.test(item)
        """
        if isinstance(item, str):
            return True
        else:
            return False

    def run_test(self, the_list):
        """
            Purpose: To only return the items from a list that are a string
            Input:   Item - A list of items
            Output:  A list of the strings from the input.
            Syntax:  <Filter object>.run_test(item)
        """
        self.logger.debug('Extracting only strings from list.')
        return list(filter(Filter.test, the_list))


def main():
    import logging

    # Condition and start logger.
    logger = logging.getLogger('main')
    logger.setLevel(logging.DEBUG)      # This can be changed as needed, for right now debug is good.
    file_handler = logging.FileHandler('filter.log')
    log_format = logging.Formatter('%(levelname)s - %(asctime)s - %(name)s - %(message)s')
    file_handler.setFormatter(log_format)
    logger.addHandler(file_handler)

    # Note in the logger that script has started.
    logger.info('*** Start ***')

    # Create an instance of the filter class.
    filter = Filter(logger)

    # Demonstrate use of some of the functions within the class.
    print('Filter a list to only return the strings:')
    the_vals = ['a', 5, 'b', 56, 12.3, [12, 3, 'b']]
    print('  Input = {}'.format(the_vals))
    print('  Output = {}'.format(filter.run_test(the_vals)))
        
    # Note in the logger that script has ended.
    logger.info('*** End ***')

    
if __name__ == "__main__":
    main()

