#!/usr/bin/env python3
"""
    Purpose: To demonstrate a FIFO and LIFO queues.
    Syntax:  python queue.py
    Use: Free use, no licensing required.
    Details: xTreemAnalytics.  Copyright 2020 by J. Cartmell.
"""

from collections import deque

# Add an element to the left of a list.
def push_element_left(list_to_update, item_to_add):
    from collections import deque
    the_queue = deque(list_to_update)
    the_queue.appendleft(item_to_add)
    return list(the_queue)

# Add an element to the right of a list.
def push_element_right(list_to_update, item_to_add):
    from collections import deque
    the_queue = deque(list_to_update)
    the_queue.append(item_to_add)
    return list(the_queue)

# Pop an element from the left of a list.
def pop_element_left(list_to_pop):
    from collections import deque
    the_queue = deque(list_to_pop)
    try:
        item = the_queue.popleft()
    except:
        item = None
    return list(the_queue), item

# Pop an element from the right of a list.
def pop_element_right(list_to_pop):
    from collections import deque
    the_queue = deque(list_to_pop)
    try:
        item = the_queue.pop()
    except:
        item = None
    return list(the_queue), item

# Initialize the list.
the_list = []
print('Initial list is {}'.format(the_list))

## FIFO example, append to the right, pop from the left.
# Append numbers to the right of a list.
for item in range(1,10):
    the_list = push_element_right(the_list, item)
    print('Appended {} to the right in {}'.format(item, the_list))

# Pop numbers from the left of the list.
for item in range(1,11):
    the_list, popped_item = pop_element_left(the_list)
    print('Popped {} from the left, remaining is {}'.format(popped_item, the_list))


# Initialize the list.
the_list = []
print('Initial list is {}'.format(the_list))

## LIFO example, append to the right, pop from the right.
# Append numbers to the right of a list.
for item in range(1,10):
    the_list = push_element_right(the_list, item)
    print('Appended {} to the right in {}'.format(item, the_list))

# Pop numbers from the right of the list.
for item in range(1,11):
    the_list, popped_item = pop_element_right(the_list)
    print('Popped {} from the right, remaining is {}'.format(popped_item, the_list))

