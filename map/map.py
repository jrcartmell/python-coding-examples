class StringMapper:
    """
        Purpose: To demonstrate the use of the Python map feature, applying it to a list of items.
        Input:   Self, logger
        Output:  None
        Syntax:  StringMapper.{Method-in-class}(self, logger)
    """
    def __init__(self, logger):
        # Store logger to be used in this instance of the class.
        self.logger = logger

    def title(item):
        """
            Purpose: To convert an item to Title case
            Input:   Item - A string to be converted to title case.
            Output:  The string in title case.
            Syntax:  <String Mapper object>.title(item)
        """
        return(str.title(item))

    def make_titles(self, the_list):
        """
            Purpose: To convert a list of item to Title case
            Input:   Item - A list to be converted to title case.
            Output:  The list whose items are converted to title case.
            Syntax:  <String Mapper object>.make_titles(item)
        """
        self.logger.debug('Converting list to Title case.')
        return list(map(StringMapper.title, the_list))

    def upper(item):
        """
            Purpose: To convert an item to Upper case
            Input:   Item - A string to be converted to upper case.
            Output:  The string in upper case.
            Syntax:  <String Mapper object>.upper(item)
        """
        return(str.upper(item))

    def make_uppers(self, the_list):
        """
            Purpose: To convert a list of item to Upper case
            Input:   Item - A list to be converted to upper case.
            Output:  The list whose items are converted to upper case.
            Syntax:  <String Mapper object>.make_uppers(item)
        """
        self.logger.debug('Converting list to Upper case.')
        return list(map(StringMapper.upper, the_list))


def main():
    import logging

    # Condition and start logger.
    logger = logging.getLogger('main')
    logger.setLevel(logging.DEBUG)      # This can be changed as needed, for right now debug is good.
    file_handler = logging.FileHandler('map.log')
    log_format = logging.Formatter('%(levelname)s - %(asctime)s - %(name)s - %(message)s')
    file_handler.setFormatter(log_format)
    logger.addHandler(file_handler)

    # Note in the logger that script has started.
    logger.info('*** Start ***')

    # Create an instance of the mapper class.
    stringMapper = StringMapper(logger)

    # Demonstrate use of some of the functions within the class.
    print('Convert a list to title case:')
    my_friends = ['scott', 'jeff', 'andrew']
    print('  Input = {}'.format(my_friends))
    print('  Output = {}'.format(stringMapper.make_titles(my_friends)))
    
    print('Convert a list to upper case:')
    my_friends = ['scott', 'jeff', 'andrew']
    print('  Input = {}'.format(my_friends))
    print('  Output = {}'.format(stringMapper.make_uppers(my_friends)))
    
    # Note in the logger that script has ended.
    logger.info('*** End ***')

    
if __name__ == "__main__":
    main()    
