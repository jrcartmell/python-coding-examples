"""
    Purpose: Script to demonstrate use of h3 library.  Create a number of locations 
             and resolve them to the h3 library zone.  Requires h3 library to be
             installed.
    Input:   None
    Output:  Two CSV files - summary.txt which shows number of tickets per zone and
             tickets.txt which shows details of each ticker.
    Syntax:  uber-h3-library.py
    Details: xTreem Analytics.  Copyright 2020 by J. Cartmell.
"""
import operator
import random
from h3 import h3

# Just pick a location where the tickets are based.
lat_base = 42.45
lon_base = -75.41

# Config for list of tickets.
number_of_tickets = 100
tickets = []
ticket_number = 1

# Create a set of random tickets.  Each ticket has a located that is somewhere near the base
# location set above.  
for i in range(number_of_tickets):
    new_entry = {}
    lon = lon_base + random.random()
    new_entry['lat'] = lat_base + random.random()/10
    new_entry['lon'] = lon_base + random.random()/10
    new_entry['zone'] = ''
    new_entry['id'] = ticket_number;
    tickets.append(new_entry)
    ticket_number += 1

# Set the h3 library initial depth - this is the resolution can be 1 to 15.
depth = 6

# Set some flags used while the code determines the proper depth/resolution.
done = False
max_tickets = 20
max_tries = 5
tries = 0

# Iterate until we get the required density.
while not done:    
    # Initialize the list of zones.
    zones = []
    
    # Figure out the zone each ticket resides in based on the h3 library and the current depth.
    for item in tickets:
        item['zone'] = h3.geo_to_h3(item['lat'], item['lon'], depth)
        if item['zone'] not in zones:
            zones.append(item['zone'])
        
    # Create a summary of the quantity of tickets in each zone.
    summary = []
    for zone in zones:
        new_entry = {}
        new_entry['zone'] = zone
        new_entry['count'] = 0
        new_entry['lat'], new_entry['lon'] = h3.h3_to_geo(zone)
        for ticket in tickets:
            if zone == ticket['zone']:
                new_entry['count'] += 1
        summary.append(new_entry)

    # If we have exceeded the maximum attempts, check if the density meets the criteria.
    # If not, then increment the depth and try again.
    done = True
    if tries < max_tries:
        for item in summary:
            if item['count'] > max_tickets:
                done = False

    tries += 1
    depth += 1
    
# Getting to here indicates that we have either the required density or have 
# the number of attempts.

# Sort the tickets by zone.
sorted_tickets = sorted(tickets, key=operator.itemgetter('zone')) 

# Write a file of the list of tickets, sorted by zone.
f = open('tickets.txt', "w")
f.write('ticket number, lat, lon, zone\n')
the_list = ['id', 'lat', 'lon', 'zone']
for item in sorted_tickets:
    for entry in the_list:
        f.write(str(item[entry]))
        f.write(', ')
    f.write('\n')
f.close()

# Write the summary out to a file.
f = open('summary.txt', "w")
f.write('zone, lat, lon, count\n')
the_list = ['zone', 'lat', 'lon', 'count']
for item in summary:
    for entry in the_list:
        f.write(str(item[entry]))
        f.write(', ')
    f.write('\n')
f.close()
