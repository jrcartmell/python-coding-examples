class Reduce:
    """
        Purpose: To demonstrate the use of the Python reduce feature, applying it to a list of items.
        Input:   Self, logger
        Output:  None
        Syntax:  Reduce.{Method-in-class}(self, logger)
    """
    def __init__(self, logger):
        # Store logger to be used in this instance of the class.
        self.logger = logger

    def the_sum(first, second):
        """
            Purpose: To add two numbers
            Input:   Two numbers.
            Output:  The sum of the two inputs.
            Syntax:  <Reduce object>.the_sum(item)
        """
        return first + second

    def custom_sum(self, the_list):
        """
            Purpose: To convert a list of numbers to their sum.
            Input:   Item - A list of numbers to be converted to their sum
            Output:  The sum
            Syntax:  <Reduce object>.custom_sum(item)
        """
        from functools import reduce
        self.logger.debug('Calculating sum of list of numbers.')
        return reduce(Reduce.the_sum, the_list)

    def the_product(first, second):
        """
            Purpose: To multipy two numbers
            Input:   Two numbers
            Output:  The product of the two inputs.
            Syntax:  <Reduce object>.the_product(item)
        """
        return first * second

    def custom_product(self, the_list):
        """
            Purpose: To convert a list of numbers to their sum.
            Input:   Item - A list of numbers to be converted to their product.
            Output:  The list whose items are converted to their product.
            Syntax:  <Reduce object>.custom_product(item)
        """
        from functools import reduce
        self.logger.debug('Calculating product of list of numbers.')
        return reduce(Reduce.the_product, the_list)


class Filter:
    """
        Purpose: To demonstrate the use of the Python filter feature, applying it to a list of items.
        Input:   Self, logger
        Output:  None
        Syntax:  Filter.{Method-in-class}(self, logger)
    """
    def __init__(self, logger):
        # Store logger to be used in this instance of the class.
        self.logger = logger

    def test(item):
        """
            Purpose: To test if an item is a number, if so, return true, otherwise return false
            Input:   Item - To be determined if number or not
            Output:  True (if a number), False (Otherwise)
            Syntax:  <Filter object>.test(item)
        """
        try:
            float(item)
            return True
        except:
            return False

    def run_test(self, the_list):
        """
            Purpose: To only return the items from a list that are numbers
            Input:   Item - A list of items
            Output:  A list of the numbers from the input.
            Syntax:  <Filter object>.run_test(item)
        """
        self.logger.debug('Extracting only numbers from list.')
        return list(filter(Filter.test, the_list))


def main():
    import logging

    # Condition and start logger.
    logger = logging.getLogger('main')
    logger.setLevel(logging.DEBUG)      # This can be changed as needed, for right now debug is good.
    file_handler = logging.FileHandler('reduce.log')
    log_format = logging.Formatter('%(levelname)s - %(asctime)s - %(name)s - %(message)s')
    file_handler.setFormatter(log_format)
    logger.addHandler(file_handler)

    # Note in the logger that script has started.
    logger.info('*** Start ***')

    # Create an instance of the filter and reduce classes.
    filter = Filter(logger)
    reduce = Reduce(logger)

    # Demonstrate use of some of the functions within the classes.
    print('Filter a list to only return the numbers using filter concept:')
    the_input = ['a', 5, 'b', 56, 12.3, [12, 3, 'b'], 56 ]
    the_numbers = filter.run_test(the_input)
    print('  Input = {}'.format(the_input))
    print('  Numbers = {}'.format(the_numbers))
    print('Then find their sum using the reduce concept:')
    print('  Output = {:,}'.format(reduce.custom_sum(the_numbers)))
    print('And finally their product using the reduce concept:')
    print('  Output = {:,}'.format(reduce.custom_product(the_numbers)))
        
    # Note in the logger that script has ended.
    logger.info('*** End ***')

    
if __name__ == "__main__":
    main()

