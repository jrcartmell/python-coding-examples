#!/usr/bin/env python3
"""
    Purpose: To parse a pcap file to extract DNS statistics.
    Syntax:  python dns-parser.py <pcap file>
    Use: Free use, no licensing required.
    Details: xTreemAnalytics.  Copyright 2020 by J. Cartmell.
"""

import logging
import os
import sys

from scapy.all import rdpcap

LOG = logging.getLogger('main')

# Helper defines
UDP_PROTOCOL = 17
TCP_PROTOCOL = 6
DNS_PORT = 53
UDP_RESPONSE_CODE_POS = 45
TCP_RESPONSE_CODE_POS = 71
UDP_DOMAIN_POS = 54
TCP_DOMAIN_POS = 80
IP_LEN_POS = 16
UDP_ANSWER_POS = 48
TCP_ANSWER_POS = 74
IP_PROTOCOL_POS = 23
SRCE_PORT_POS = 34
DEST_PORT_POS = 36
TCP_FLAGS_POS = 47
TCP_FLAGS_MASK = 24
IP_PACKET_ID_POS = 18
IP_SRCE_POS = 26
IP_DEST_POS = 30

# Helper function to determine if a packet is a UDP DNS Request.
def isUdpDnsRequest(pkt):
    # If message is a UDP message to the DNS port, return true.
    return pkt[IP_PROTOCOL_POS] == UDP_PROTOCOL and \
           pkt[DEST_PORT_POS]*256 + pkt[DEST_PORT_POS+1] == DNS_PORT \
           if True else False

# Helper function to determine if a packet is a UDP DNS Response.
def isUdpDnsResponse(pkt):
    # If message is a UDP message from the DNS port, return true.
    return pkt[IP_PROTOCOL_POS] == UDP_PROTOCOL and \
           pkt[SRCE_PORT_POS]*256 + pkt[SRCE_PORT_POS+1] == DNS_PORT \
           if True else False

# Helper function to determine if a packet is a TCP DNS Request.
def isTcpDnsRequest(pkt):
    # If message is a TCP message to the DNS port, return true.
    # Check that message has the TCP data, i.e. ignore syn and syn, ack msgs.
    return pkt[IP_PROTOCOL_POS] == TCP_PROTOCOL and \
           pkt[DEST_PORT_POS]*256 + pkt[DEST_PORT_POS+1] == DNS_PORT and \
           pkt[TCP_FLAGS_POS] == TCP_FLAGS_MASK \
           if True else False

# Helper function to determine if a packet is a TCP DNS Response.
def isTcpDnsResponse(pkt):
    # If message is a TCP message from the DNS port, return true.
    # Check that message has the TCP data, i.e. ignore syn and syn, ack msgs.
    return pkt[IP_PROTOCOL_POS] == TCP_PROTOCOL and \
           pkt[SRCE_PORT_POS]*256 + pkt[SRCE_PORT_POS+1] == DNS_PORT and \
           pkt[TCP_FLAGS_POS] == TCP_FLAGS_MASK \
           if True else False

# Helper function to extract length of IP packet.
def getIpLen(pkt):
    # Extract the length of the IP packet.
    return pkt[IP_LEN_POS]*256 + pkt[IP_LEN_POS+1]

# Helper function to extract response code from DNS response message.
def extractResponseCode(pkt, pos, r_codes):
    # Extract response code and add to response code dictionary counts.
    the_response_code = pkt[pos] & 31
    try:
        r_codes[the_response_code] += 1
    except:
        r_codes[the_response_code] = 1

# Helper function to extract domain name from DNS request message.        
def extractDomain(pkt, pos, domain_reqs):
    # Traverse through request to reconstruct the domain name.
    indy = pos
    the_req = b''
    while(pkt[indy] !=0):
        the_len = pkt[indy]
        the_string = pkt[indy + 1: indy + 1 + the_len]
        the_req = the_req + the_string + b'.'
        indy = indy + 1 + the_len
    try:
        domain_reqs[the_req.decode('utf-8')] += 1
    except:
        domain_reqs[the_req.decode('utf-8')] = 1

# Helper function to extract number of answers from DNS response message.
def extractNumAnswers(pkt, pos):
    # Extract number of answers from response.
    return pkt[pos]*256 + pkt[pos+1]

# Helper function to extract info of the packet with the max number of answers.
def extractMaxAnswerInfo(pkt):
    max_srce = str(pkt[IP_SRCE_POS]) + '.' + str(pkt[IP_SRCE_POS+1]) + '.' + \
               str(pkt[IP_SRCE_POS+2]) + '.' + str(pkt[IP_SRCE_POS+3])
    max_dest = str(pkt[IP_DEST_POS]) + '.' + str(pkt[IP_DEST_POS+1]) + '.' + \
               str(pkt[IP_DEST_POS+2]) + '.' + str(pkt[IP_DEST_POS+3])
    max_id = pkt[IP_PACKET_ID_POS]*256 + pkt[IP_PACKET_ID_POS+1]
    return max_srce, max_dest, max_id

def main():

    try:
        pcap_filename = sys.argv[1]
    except IndexError:
        print('Usage: python %s <pcap file>' % os.path.basename(sys.argv[0]))
        sys.exit(1)
    try:
        packets = rdpcap(pcap_filename)
    except:
        print('Something is amiss with trying to access pcap file.')
        sys.exit(1)

    LOG.info('Loaded %s: %r', pcap_filename, packets)

    # Initialize items needed to generate the required information/stats.
    r_codes = {}    # Dictionary of r codes.
    r_code_map = {0: 'No Error', 
                  1: 'Format Error', 
                  2: 'Server Failure',
                  3: 'Non-Existent Domain'}     # List of response codes.
    domain_reqs = {}    # Dictionary of which domains were requested.
    max_ancount = 0     # Max answer RR.
    max_id = 0          # Id of packet with max answer RR.
    max_srce = ''       # Source IP address of packet with max answer RR.
    max_dest = ''       # Destination IP address of packet with max answer RR.
    list_of_msg_lens = []   # List of message lengths.
    top_domain_cutoff = 5   # Top 5 domains.  If top 10, change to 10, etc.
    dns_udp_req = 0     # Count of DNS UDP Requests
    dns_udp_res = 0     # Count of DNS UDP Responses
    dns_tcp_req = 0     # Count of DNS TCP Requests
    dns_tcp_res = 0     # Count of DNS TCP Responses
    
    for pkt in packets:
        LOG.debug("Visiting packet: %r", pkt)
        # TODO: Your code can start here.

        # Recast pkt as bytes since we are not using scapy library.
        pkt = bytes(pkt)

        # Determine if the packet is a DNS packet.  If so, process it.
        # DNS packet can be in either TCP or UDP and can either be 
        # request or response.  So handle each case.
        if isUdpDnsRequest(pkt):
            # UDP DNS Request, increment count.
            dns_udp_req += 1

            # Append message length to list of msg lengths.
            list_of_msg_lens.append(getIpLen(pkt))

            # Extract domain name from request.
            extractDomain(pkt, UDP_DOMAIN_POS, domain_reqs)
            
        elif isUdpDnsResponse(pkt):
            # UDP DNS Response, increment count.
            dns_udp_res += 1

            # Append message length to list of msg lengths.
            list_of_msg_lens.append(getIpLen(pkt))
    
            # Extract response code from response.
            extractResponseCode(pkt, UDP_RESPONSE_CODE_POS, r_codes)

            # Extract number of answers in response.
            answer_rr = extractNumAnswers(pkt, UDP_ANSWER_POS)
            if answer_rr > max_ancount:
                max_ancount = answer_rr
                max_srce, max_dest, max_id = extractMaxAnswerInfo(pkt)

        elif isTcpDnsRequest(pkt):
            # TCP DNS Request, increment count.
            dns_tcp_req += 1

            # Append message length to list of msg lengths.
            list_of_msg_lens.append(getIpLen(pkt))

            # Extract domain name from request.
            extractDomain(pkt, TCP_DOMAIN_POS, domain_reqs)
            
        elif isTcpDnsResponse(pkt):
            # TCP DNS Response, increment count.
            dns_tcp_res += 1
         
            # Append message length to list of msg lengths.
            list_of_msg_lens.append(getIpLen(pkt))

            # Extract response code from response.
            extractResponseCode(pkt, TCP_RESPONSE_CODE_POS, r_codes)

            # Extract number of answers in the response.
            answer_rr = extractNumAnswers(pkt, TCP_ANSWER_POS)
            if answer_rr > max_ancount:
                max_ancount = answer_rr
                max_srce, max_dest, max_id = extractMaxAnswerInfo(pkt)

    # Get the number of messages, used several times below so only do once.
    num_of_dns = dns_udp_req + dns_udp_res + dns_tcp_req + dns_tcp_res 

    LOG.info('Number of DNS messages = {}.'.format(num_of_dns))
    LOG.info('')
    
    LOG.info('Response Code Histogram:')
    the_keys = r_codes.keys()
    for item in the_keys:
        LOG.info('  Response code {} ({}) was observed {} times.'.format(item, \
                 r_code_map[item], r_codes[item]))
    LOG.info('')

    try:
        LOG.info('Average DNS message size = {}.'. \
                 format(round(sum(list_of_msg_lens)/num_of_dns),2))
    except ZeroDivisionError as error:
        LOG.info('Since there were no DNS messages, there is no average DNS message size.')
    LOG.info('')
    
    # Calculate the median.
    list_of_msg_lens.sort()
    try:
        if num_of_dns % 2 == 0:
            med1 = list_of_msg_lens[num_of_dns//2]
            med2 = list_of_msg_lens[num_of_dns//2 - 1]
            median = (med1+med2)/2
        else:
            median = list_of_msg_lens[num_of_dns//2]    
        LOG.info('Median DNS message size = {}.'.format(round(median,2)))
    except:
        LOG.info('Problem with computing median DNS message size.')
    LOG.info('')

    # Convert the requested domains dictionary into a list so we can figure
    # out which domain was most requested.
    domain_list = []
    for key,val in domain_reqs.items():
        domain_list.append({'domain':key, 'count':val})

    # Sort the dictionary.
    domain_list = sorted(domain_list, key=lambda x: x['count'], reverse=True)

    # Figure out where the cutoff is for the top domains. This handlea ties.
    cutoff_count = domain_list[top_domain_cutoff-1]['count']

    # Display the top domains along with their count.
    iter = 0
    len_of_domain_reqs = len(domain_reqs)
    LOG.info('Most requested domains:')
    while iter < len_of_domain_reqs and \
          domain_list[iter]['count'] >= cutoff_count:
        LOG.info('  Domain {} requested {} times.'.format(domain_list[iter]['domain'], \
                                                          domain_list[iter]['count']))
        iter += 1
    LOG.info('')
        
    LOG.info('DNS response with most ({}) answer rrs is IP id = {} from {} to {}.'.   \
             format(max_ancount, max_id, max_srce, max_dest))
    LOG.info('')
    
    LOG.info('Done.')
    
if __name__ == "__main__":
    logging.basicConfig(
        format="[%(funcName)s] %(message)s",
        level=logging.INFO)
    main()
