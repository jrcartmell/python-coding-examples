#!/usr/bin/env python3
"""
    Purpose: To demonstrate a Monte Carlo simulation for sales commissions.
    Syntax:  python monte.py
    Use: Free use, no licensing required.
    Details: xTreemAnalytics.  Copyright 2021 by J. Cartmell.
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Set a slew of parameters used in the simulations
per_rep_achieved_avg = 1
per_rep_achieved_std_dev = .075
no_sales_reps = 1000
no_simulations = 100000
lower_percentile = 5
upper_percentile = 95
no_bins = 50
interactive_mode = True

# Predicted sales per sales rep amounts and distribution
per_rep_sales_target_values = [100000, 110000, 120000, 130000, 140000]
per_rep_sales_target_prob = [0.05, 0.2, 0.5, 0.2, 0.05]

# Define a small function to calculate the commission rate based on the sales.
def determine_commission_rate(x):
    """ Return the commission rate based on the table:
    1% commission for 0-50% of target sales achieved
    2% commission for 51-90% of target sales achieved
    3% commission for 91-99% of target sales achieved
    4% commission for 100% or higher of target sales achieved
    """
    if x <= 0.50:
        return 0.01
    elif x <= 0.90:
        return 0.02
    elif x <= 0.99:
        return 0.03
    else:
        return 0.04
        
# Define a list to keep all the results from each simulation that we want to analyze
cum_stats = []

# Perform the requisite number of simulations.
for i in range(no_simulations):

    # Choose random inputs for the sales targets and percent to target
    # The sales targets come from the historical values computed elsewhere.
    # The achieved percentage to target is based on a normal distribution.  This
    # too is based on historical empirical evidence gleaned elsewhere.
    per_rep_sales_target = np.random.choice(per_rep_sales_target_values, 
                                            no_sales_reps, 
                                            p=per_rep_sales_target_prob)
    per_rep_pct_to_target = np.random.normal(per_rep_achieved_avg, 
                                             per_rep_achieved_std_dev, 
                                             no_sales_reps).round(2)
    
    # Build the dataframe based on the inputs and number of reps
    df = pd.DataFrame(index=range(no_sales_reps), data={'Pct_To_Target': per_rep_pct_to_target,
                                                        'Sales_Target': per_rep_sales_target})
    
    # For each rep, determine the actual amount of sales based on the sales target and the percent to target rate
    df['Sales_Achieved'] = df['Pct_To_Target'] * df['Sales_Target']
    
    # For each rep, determine the commission rate based on the actual sales 
    # and then determine the amount of commission.
    df['Commission_Rate'] = df['Pct_To_Target'].apply(determine_commission_rate)
    df['Commission_Amount'] = df['Commission_Rate'] * df['Sales_Achieved']
    
    # Track sales, commission amounts, sales targets and sales as a percentage of target 
    # over all the reps in this instance of the simulation.  Store in a structure so
    # a histogram can be presented for each.
    cum_stats.append([df['Sales_Achieved'].sum().round(2),
                      df['Commission_Amount'].sum().round(2),
                      df['Sales_Target'].sum().round(2),
                      df['Pct_To_Target'].mean().round(2)])
           
# Create a data frame based on the results for each 
results_df = pd.DataFrame.from_records(cum_stats, columns=['Sales_Achieved',
                                                           'Commission_Amount',
                                                           'Sales_Target',
                                                           'Pct_To_Target'])
                                                           
# Generated plots

# Create and save a histogram plot for the sales achieved.
plt.clf()
plt.hist(results_df['Sales_Achieved'], bins=no_bins)
plt.axvline(np.percentile(results_df['Sales_Achieved'],lower_percentile), color='r', linestyle='dashed', linewidth=2)
plt.axvline(np.percentile(results_df['Sales_Achieved'],upper_percentile), color='r', linestyle='dashed', linewidth=2)
plt.title('Total Sales Achieved Distribution')
plt.xlabel('Total Sales Achieved ($)')
plt.ylabel('Occurrences')
if interactive_mode:
    plt.show()
plt.savefig('Total Sales.png')

# Create and save a histogram for the sales target.
plt.clf()
plt.hist(results_df['Sales_Target'], bins=no_bins)
plt.axvline(np.percentile(results_df['Sales_Target'],lower_percentile), color='r', linestyle='dashed', linewidth=2)
plt.axvline(np.percentile(results_df['Sales_Target'],upper_percentile), color='r', linestyle='dashed', linewidth=2)
plt.title('Sales Target Distribution')
plt.xlabel('TargetSales ($)')
plt.ylabel('Occurrences')
if interactive_mode:
    plt.show()
plt.savefig('Target Sales.png')

# Create and save a histogram of the commission amount.
plt.clf()
plt.hist(results_df['Commission_Amount'], bins=no_bins)
plt.axvline(np.percentile(results_df['Commission_Amount'],lower_percentile), color='r', linestyle='dashed', linewidth=2)
plt.axvline(np.percentile(results_df['Commission_Amount'],upper_percentile), color='r', linestyle='dashed', linewidth=2)
plt.title('Commissions Earned')
plt.xlabel('Commissions Earned ($)')
plt.ylabel('Occurrences')
if interactive_mode:
    plt.show()
plt.savefig('Commision Amount.png')

# Create and save a histogram of the percentage of target sales that was achieved.
plt.clf()
plt.hist(results_df['Pct_To_Target'], bins=no_bins)
plt.axvline(np.percentile(results_df['Pct_To_Target'],lower_percentile), color='r', linestyle='dashed', linewidth=2)
plt.axvline(np.percentile(results_df['Pct_To_Target'],upper_percentile), color='r', linestyle='dashed', linewidth=2)
plt.title('Sales Achieved/Target Sales')
plt.xlabel('Sales Achieved/Target Sales')
plt.ylabel('Occurrences')
if interactive_mode:
    plt.show()
plt.savefig('Percentage To Target.png')


