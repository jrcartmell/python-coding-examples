#!/usr/bin/python3
"""
    Purpose: To get and display the current price of a cryptocurency.
             You need to go to https://coinmarketcap.com/ to get an API key.
             And you need to know the symbol of the currency you want a quote, 
             like BTC for bitcoin and ETH for Etherium.
    Input:   An API key and the symbol of the currency you want a price for.
    Output:  The price of the cryptocurrency.
    Syntax:  python crypto.py <API key> <crypto symbol>
    Details: xTreem Analytics.  Copyright 2020 by J. Cartmell.
"""

import json
import os
import requests
import sys
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects

def main():
    # Set the URL to get the quote.
    url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'

    # Extract the API key from the command line.
    try:
        api_key = sys.argv[1]
    except IndexError:
        print('Usage: python %s <api key> <symbol>' % os.path.basename(sys.argv[0]))
        sys.exit(1)

    # Extract the stock symbol from the command line.
    try:
        symbol = sys.argv[2]
    except IndexError:
        print('Usage: python %s <api key> <symbol>' % os.path.basename(sys.argv[0]))
        sys.exit(1)

    # Set the header that must be sent with the API request.  Specificially, the API key.
    header = {
        'Accepts': 'application/json',
        'X-CMC_PRO_API_KEY': api_key,
    }

    # Set the parameters to be sent with the API request.  In this case, it is the 
    # symbol for which a quote is desired.
    parameters = {
        'symbol':symbol
    }

    # Create an HTTP session
    session = requests.Session()

    # Set the header.
    session.headers.update(header)
    
    try:
        # Query the server and process the request.  If there is an error, print it and exit.
        response = session.get(url, params=parameters)
        data = json.loads(response.text)
        print('{} is at ${:,} at time {}.'.format(symbol, round(data['data'][symbol]['quote']['USD']['price'],4),
              data['data'][symbol]['quote']['USD']['last_updated']))
    except (ConnectionError, Timeout, TooManyRedirects) as e:
        print(e)
  

if __name__ == "__main__":
    main()
    
